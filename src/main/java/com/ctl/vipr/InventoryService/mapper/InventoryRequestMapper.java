package com.ctl.vipr.InventoryService.mapper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctl.ecaps.serviceobject.AtmFilterSO;
import com.ctl.ecaps.serviceobject.CeFilterSO;
import com.ctl.ecaps.serviceobject.CugFilterSO;
import com.ctl.ecaps.serviceobject.DedicatedLdFilterSO;
import com.ctl.ecaps.serviceobject.DiaFilterSO;
import com.ctl.ecaps.serviceobject.DplFilterSO;
import com.ctl.ecaps.serviceobject.DwhFilterSO;
import com.ctl.ecaps.serviceobject.ElineFilterSO;
import com.ctl.ecaps.serviceobject.EntitlementSO;
import com.ctl.ecaps.serviceobject.EplFilterSO;
import com.ctl.ecaps.serviceobject.EspFilterSO;
import com.ctl.ecaps.serviceobject.EvcMemberFilterSO;
import com.ctl.ecaps.serviceobject.FilterConditionSO;
import com.ctl.ecaps.serviceobject.FilterExpressionSO;
import com.ctl.ecaps.serviceobject.FrameFilterSO;
import com.ctl.ecaps.serviceobject.InventoryFilterSO;
import com.ctl.ecaps.serviceobject.InventoryLocationFilterSO;
import com.ctl.ecaps.serviceobject.InventoryRequestSO;
import com.ctl.ecaps.serviceobject.IplFilterSO;
import com.ctl.ecaps.serviceobject.IqFilterSO;
import com.ctl.ecaps.serviceobject.LdFilterSO;
import com.ctl.ecaps.serviceobject.LsFilterSO;
import com.ctl.ecaps.serviceobject.MfwVpnFilterSO;
import com.ctl.ecaps.serviceobject.MoeFilterSO;
import com.ctl.ecaps.serviceobject.MsFilterSO;
import com.ctl.ecaps.serviceobject.NbsFilterSO;
import com.ctl.ecaps.serviceobject.NmFilterSO;
import com.ctl.ecaps.serviceobject.OwsFilterSO;
import com.ctl.ecaps.serviceobject.PageSO;
import com.ctl.ecaps.serviceobject.PlFilterSO;
import com.ctl.ecaps.serviceobject.QrouteFilterSO;
import com.ctl.ecaps.serviceobject.RelatedInventoryFilterSO;
import com.ctl.ecaps.serviceobject.SortSO;
import com.ctl.ecaps.serviceobject.TollFreeFilterSO;
import com.ctl.ecaps.serviceobject.VoipFilterSO;
import com.ctl.vipr.InventoryService.model.InventoryRequest;
import com.ctl.vipr.InventoryService.model.QciEnums.ProductTypeCode;
import com.ctl.vipr.InventoryService.model.SearchFilter;
import com.ctl.vipr.InventoryService.model.SearchFilterCondition;
import com.ctl.vipr.InventoryService.model.SearchFilterExpression;
import com.ctl.vipr.InventoryService.model.SearchOptions;
import com.ctl.vipr.InventoryService.model.VSortInfo;
import com.ctl.vipr.InventoryService.model.VStringUtils;

public class InventoryRequestMapper {

	private static Logger log = LoggerFactory.getLogger(InventoryRequestMapper.class);

	public InventoryRequestSO map(String request, InventoryRequest req) {		
		final String enterpriseId = req.getEnterpriseId();
		/*This below userid field not using in the request mapping*/
		final String userId = req.getUserId();
		/*This we want to get inventory items by userid and enterpriseid combination, 
		 * specify the userid in the search string like : productTypeCode:IQNET AND UserId:49089*/
		final String searchString = req.getSearchString();
		final String pageInfo = req.getPageInfo();
		final String sortInfo = req.getSortInfo();

		InventoryRequestSO inventoryRequest = new InventoryRequestSO();
		EntitlementSO inventoryEntitlement = new EntitlementSO();

		// Identifing the UserId field and its value
		// Identifing the UserId field and its value
		//Pattern pattern = Pattern.compile("(?<=AND\\s|^)UserId:(.*?)(?=\\sAND|$)");
		//Matcher matcher = pattern.matcher(searchString);
		//if (matcher.find()) {
		//***commenting the above to remove the hack of sending userid through search string,now directly using userid from restauto***
			inventoryEntitlement.setUserId(new Long(userId));
		//}
		String productTypeCode = null;
		Pattern productTypeCodePattern = Pattern.compile("(?<=AND\\s|^)productTypeCode:(.*?)(?=\\sAND|$)");
		Matcher productTypeCodematcher = productTypeCodePattern.matcher(searchString);
		if (productTypeCodematcher.find()) {
			String productTypeCodeStr = productTypeCodematcher.group(1);
			if(!productTypeCodeStr.startsWith("(") && !productTypeCodeStr.endsWith(")")){
				productTypeCode = productTypeCodematcher.group(1);
			}
			if(log.isDebugEnabled())
				log.debug("productTYpeCode value from Search string :: {}",productTypeCode);
		}
		
		inventoryEntitlement.setEnterpriseId(Long.parseLong(enterpriseId));
		inventoryRequest.setEntitlement(inventoryEntitlement);

		inventoryRequest.setPageData(mapPageSO(pageInfo, sortInfo));
		SearchOptions searchOptions = new SearchOptions(searchString);
//		if(log.isDebugEnabled())
//			log.debug("searchOptions --{}",xstream.toXML(searchOptions));
		FilterExpressionSO mainFilterExpr = mapFilterExpression(searchOptions, productTypeCode);


		//Set the filter expression only if there is one.
		if(mainFilterExpr != null){
			inventoryRequest.setFilterExpression(mapFilterExpression(searchOptions, productTypeCode));
		}
		return inventoryRequest;
	}

	private void populateLocationSortList(List<SortSO> sortSOList, String sortColumnStr, String sortDirection , String secSortColumnStr,  String terSortColumnStr){
		String sortColVal=null;
		String secSortColVal=null;
		String terSortColVal=null;
		if(sortColumnStr!=null){
			sortColVal=sortColumnStr.split("\\.")[1] ;
		}
		if(secSortColumnStr!=null){
			secSortColVal=secSortColumnStr.split("\\.")[1] ;
		}
		if(terSortColumnStr!=null){
			terSortColVal=terSortColumnStr.split("\\.")[1] ;
		}
		String sortColLoc="location";
		String []colArr = {"addressLine1","addressLine2","addressLine3", "city","stateProvinceCode","postalCode","countryCode"};
		List<String> colList = new ArrayList<String>(Arrays.asList(colArr));
		colList.remove(sortColVal);
		colList.remove(secSortColVal);
		colList.remove(terSortColVal);
		SortSO sortSO = null;
		for (String colListStr : colList) {
			sortSO = new SortSO();
			sortSOList.add(mapSortInfo(sortSO,sortColLoc +"."+ colListStr,sortDirection));
		}
		sortSO = new SortSO();
		sortSOList.add(mapSortInfo(sortSO,"serviceId",sortDirection));

	}


	private SortSO mapSortInfo(SortSO sortSO, String sortColumn,String sortDirection){
		sortSO.setSortColumn(sortColumn);
		sortSO.setSortDirection(sortDirection);
		return sortSO;

	}

	protected PageSO mapPageSO (String pageInfo, String sortInfo) {
		PageSO pageSO = new PageSO();
		String sortColumnStr = null;
		String sortDirection = null;
		String secSortColumnStr = null;
		String secSortDirection = null;
		String terSortColumnStr = null;
		String terSortDirection = null;
		/*
		 * code change to introduce a large page size in the inventory request, so that we get a bigger pagesize at first and then do 
		 * pagination in the response mapper before responding to the client. 
		 * Putting bigger inventory in the vipr infinispan, will let us fastly respond to the next pages of the similar request.
		 */
		//Integer fetchPageSize = propAccess.getIntProperty("qciinventory.fetch.pageSize", 1000);
		if (pageInfo != null) {
			String[] pia = pageInfo.split("\\^");
			int startIndex = Integer.parseInt(pia[0]);
			int blocksize = Integer.parseInt(pia[1]);
			/*int temp = (startIndex / fetchPageSize) * fetchPageSize;
			startIndex = temp;*/
			pageSO.setStartIndex(startIndex);
			pageSO.setBlockSize(blocksize);
			log.debug("startIndex : {}  blockSize : {} ",startIndex, blocksize);
		}
		List<SortSO> sortSOList = new ArrayList<SortSO>();
		if(sortInfo != null && sortInfo.length() > 0){
			VSortInfo sortInfoObj = new VSortInfo(sortInfo);
			if(sortInfoObj.getSortColumn() != null && !sortInfoObj.getSortColumn().equals("null")){
				SortSO sortSOPrimary = new SortSO();
				sortSOList.add(mapSortInfo(sortSOPrimary,sortInfoObj.getSortColumn(),sortInfoObj.getSortDirection()));

				if(sortInfoObj.getSecondarySortField() != null && !sortInfoObj.getSecondarySortField().equals("null")){
					SortSO sortSOSecondary = new SortSO();
					sortSOList.add(mapSortInfo(sortSOSecondary,sortInfoObj.getSecondarySortField(),sortInfoObj.getSecondarySortDirection()));
				}
				
				if(sortInfoObj.getTertiarySortField() != null && !sortInfoObj.getTertiarySortField().equals("null")){
					SortSO sortSOTertiary = new SortSO();
					sortSOList.add(mapSortInfo(sortSOTertiary,sortInfoObj.getTertiarySortField(),sortInfoObj.getTertiarySortDirection()));
				}
				
				if(SearchFilter.LOCATION_TYPE_CODE.equalsIgnoreCase(sortInfoObj.getSortColumn()) || SearchFilter.LOCATION_NAME.equalsIgnoreCase(sortInfoObj.getSortColumn())
						|| SearchFilter.LOCATION_STATE_PROVINCE.equalsIgnoreCase(sortInfoObj.getSortColumn()) || SearchFilter.LOCATION_CITY.equalsIgnoreCase(sortInfoObj.getSortColumn())
						|| SearchFilter.LOCATION_STREET.equalsIgnoreCase(sortInfoObj.getSortColumn())){
					sortColumnStr = sortInfoObj.getSortColumn();
					sortDirection = sortInfoObj.getSortDirection();
				}
				
				if(SearchFilter.LOCATION_TYPE_CODE.equalsIgnoreCase(sortInfoObj.getSecondarySortField()) || SearchFilter.LOCATION_NAME.equalsIgnoreCase(sortInfoObj.getSecondarySortField())
						|| SearchFilter.LOCATION_STATE_PROVINCE.equalsIgnoreCase(sortInfoObj.getSecondarySortField()) || SearchFilter.LOCATION_CITY.equalsIgnoreCase(sortInfoObj.getSecondarySortField())
						|| SearchFilter.LOCATION_STREET.equalsIgnoreCase(sortInfoObj.getSecondarySortField())){
					secSortColumnStr = sortInfoObj.getSecondarySortField();
					secSortDirection = sortInfoObj.getSecondarySortDirection();
				}
				
				if(SearchFilter.LOCATION_TYPE_CODE.equalsIgnoreCase(sortInfoObj.getTertiarySortField()) || SearchFilter.LOCATION_NAME.equalsIgnoreCase(sortInfoObj.getTertiarySortField())
						|| SearchFilter.LOCATION_STATE_PROVINCE.equalsIgnoreCase(sortInfoObj.getTertiarySortField()) || SearchFilter.LOCATION_CITY.equalsIgnoreCase(sortInfoObj.getTertiarySortField())
						|| SearchFilter.LOCATION_STREET.equalsIgnoreCase(sortInfoObj.getTertiarySortField())){
					terSortColumnStr = sortInfoObj.getTertiarySortField();
					terSortDirection = sortInfoObj.getTertiarySortDirection();
				}
				
				if(sortColumnStr!=null && (sortColumnStr.contains("location"))){
					populateLocationSortList(sortSOList, sortColumnStr, sortDirection, secSortColumnStr, terSortColumnStr);		
				}
			}
		}
		if(sortSOList.size() > 0){
			pageSO.getSortData().addAll(sortSOList);
		}
		return pageSO;
	}

	/*
	 * This method takes the searchOptions object as parameter and constructs the filterExpressionSO
	 * for the QCI request. SearchOptions object follows similar heirarchy as the filterExpressionSO. The SearchOptions
	 * object is parsed and the filterExpressionSO is populated in a similar way as the SearchOptions was constructed.
	 * To understand the logic in simple terms, look at the SearchOptions(String) constructor in the SearchOptions POJO
	 * Most of the main column expressions like serviceId:123fsd*;customName:Custom*;portType:IQ ENHANCED|IQ INTERNET;bandwidth:>6 Mbps;
	 * will go to the expressions list in the main FilterExpressionSO.
	 */
	protected FilterExpressionSO mapFilterExpression(SearchOptions searchOptions, String productTypeCode) {
		FilterExpressionSO mainFilterExpression = null;

		if(null != searchOptions && searchOptions.getExpression() != null ){
			mainFilterExpression = new FilterExpressionSO();
			SearchFilterExpression mainSfe = searchOptions.getExpression();
			mainFilterExpression = mapFilterExpression(mainSfe, productTypeCode);
		}
		return mainFilterExpression;
	}

	/*
	 * This method is a helper method for the mapFilterExpression method. This method is written because of the
	 * possibility of the nested expressions. For example, for the location search where nested expressions are expected
	 * this will be useful. serviceLocation:(locationStreet:930 15th Street,locationCity:Denver)|(locationStreet:123 Test Dr,locationCity:Arvada)
	 * The expression inside the first paranthesis will be one nested expression and the second one after parenthesis
	 * form the second nested expression. These two expressions form the list of expressions to one of the secondary filterExpressionSO
	 */
	protected FilterExpressionSO mapFilterExpression(SearchFilterExpression mainSfe, String productTypeCode){
		/*
		 *  Null check for mainSfe and mainSfe.expressions
		 *  If any of the above conditions satisfy, return a new filterExpressionSO. Returning null may
		 *  result in NPE as there is a high possibility of forming a filterExpressionSO with null expressions but
		 *  having conditions in it.
		 */

		if(mainSfe == null || mainSfe.getExpressions() == null)
			return new FilterExpressionSO();
		FilterExpressionSO mainFilterExpression = new FilterExpressionSO();
		mainFilterExpression.setOperator(mainSfe.getOperator().trim());
		List<FilterExpressionSO> filterExpressionList = new ArrayList<FilterExpressionSO>();
		for(int i = 0;i <= mainSfe.getExpressions().size()-1;i++){
			SearchFilterExpression secSfe = mainSfe.getExpressions().get(i);
			FilterExpressionSO secFilterExprSO = new FilterExpressionSO();
			if(secSfe.getExpressions() != null && secSfe.getExpressions().size() > 0){
				secFilterExprSO = mapFilterExpression(secSfe,productTypeCode);
			}
			secFilterExprSO.setOperator(secSfe.getOperator().trim());
			List<FilterConditionSO> secConditions = mapFilterCondition(secSfe.getConditions(),productTypeCode);
			if(secConditions != null){
				secFilterExprSO.getConditions().addAll(secConditions);
			}
			/*
			 * jxredd5: The following if block is to make sure that we dont add any empty expressions to the request.
			 */
			if(secFilterExprSO.getConditions().size() == 0 && secFilterExprSO.getExpression().size() == 0){
				continue;
			}
			filterExpressionList.add(secFilterExprSO);
		}
		List<FilterConditionSO> mainConditions = mapFilterCondition(mainSfe.getConditions(),productTypeCode);
		if(mainConditions != null){
			mainFilterExpression.getConditions().addAll(mainConditions);
		}
		if(filterExpressionList != null){
			mainFilterExpression.getExpression().addAll(filterExpressionList);
		}

		return mainFilterExpression;
	}

	/*
	 * This method is to map the conditions of a SearchFilterExpression to a FilterExpressionSO.
	 */
	protected List<FilterConditionSO> mapFilterCondition(ArrayList<SearchFilterCondition> conditions, String productTypeCode){
		if(conditions == null || conditions.size() <= 0)
			return null;
		List<FilterConditionSO> filterConditionSOList = new ArrayList<FilterConditionSO>();

		for(int j = 0;j <= conditions.size()-1;j++){
			FilterConditionSO filterConditionSO = new FilterConditionSO();
			/*
			 * JXREDD5: Added as part of the SR491378 to specify ignoreCase on filterConditions being sent to
			 * QCTRL. The requirement is to hardcode ignorecase to TRUE
			 */
			filterConditionSO.setIgnoreCase(Boolean.TRUE);
			InventoryFilterSO inventoryFilterSO = null;
			
			/*
			 * For SILO pages or assuming for cases, where productTypeCode search field has a single product value, we will use
			 * product specific filter SO, so that any product specific filter fields can be set, that are not in the inventory
			 * filter SO.
			 */
			if(productTypeCode != null){
				inventoryFilterSO = mapProductSpecificFilterSO(productTypeCode, conditions.get(j).getFilter());
			}else{
				inventoryFilterSO = new InventoryFilterSO();
			}
			
			mapCommonInventoryFilterSOFields(conditions.get(j), inventoryFilterSO);

			/*
			 * jxredd5: Fix in here to make sure we are not adding any filterCondition with empty filterSO
			 * to the condition list of the expression being built.
			 */
			if(!isInventoryFilterSOEmpty(inventoryFilterSO)|| (inventoryFilterSO.getClass().getSuperclass().equals(InventoryFilterSO.class))){
				filterConditionSO.setFilter(inventoryFilterSO);
				filterConditionSO.setConditionalOperator(conditions.get(j).getConditionalOperator());

				filterConditionSOList.add(filterConditionSO);
			}
		}
		return filterConditionSOList;
	}
	

	protected InventoryFilterSO mapCommonInventoryFilterSOFields(SearchFilterCondition condition, InventoryFilterSO inventoryFilterSO){
		
		String filterName = condition.getFilter().getFilterName().trim();
		String filterValue = condition.getFilter().getFilterValue().trim();
		String operator = condition.getConditionalOperator();
		
		InventoryLocationFilterSO location = null;
		if(SearchFilter.LOCATION_CITY.equalsIgnoreCase(filterName)){
			location = new InventoryLocationFilterSO();
			location.setCity(filterValue);
		}else if (SearchFilter.LOCATION_NAME.equalsIgnoreCase(filterName)){
			location = new InventoryLocationFilterSO();
			location.setLocationName(filterValue);
		}else if (SearchFilter.LOCATION_POSTAL_CODE.equalsIgnoreCase(filterName)){
			location = new InventoryLocationFilterSO();
			location.setPostalCode(filterValue);
		}else if (SearchFilter.LOCATION_STATE_PROVINCE.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setStateProvinceCode(filterValue);
		}else if (SearchFilter.LOCATION_CILLI_CODE.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setClliCode(filterValue);
		}else if (SearchFilter.LOCATION_COUNTRY_CODE.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setCountryCode(filterValue);
		}else if (SearchFilter.LOCATION_ADDRESS_LINE1.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setAddressLine1(filterValue);
		}else if (SearchFilter.LOCATION_ADDRESS_LINE2.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setAddressLine2(filterValue);
		}else if (SearchFilter.LOCATION_ADDRESS_LINE3.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setAddressLine3(filterValue);
		}else if (SearchFilter.LOCATION_SERVICE_ADDRESS_ID.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setServiceAddressId(filterValue);
		}else if (SearchFilter.LOCATION_TYPE_CODE.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setLocationTypeCode(filterValue);
		}else if (SearchFilter.LOCATION_ID.equalsIgnoreCase(filterName)) {
			location = new InventoryLocationFilterSO();
			location.setLocationId(Long.parseLong(filterValue));
		}
		if(location != null){
			inventoryFilterSO.setLocation(location);
		}
		
		if(SearchFilter.UBI.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setUbi(filterValue);
		}
		
		RelatedInventoryFilterSO relatedInventoryParent = null;
		if(SearchFilter.PARENT_INVENTORY_ID.equals(filterName)){
			relatedInventoryParent = new RelatedInventoryFilterSO();
			relatedInventoryParent.setInventoryId(Long.parseLong(filterValue));
		} else if(SearchFilter.PARENT_INVENTORY_TYPE_CODE.equals(filterName)){
			relatedInventoryParent = new RelatedInventoryFilterSO();
			relatedInventoryParent.setInventoryTypeCode(filterValue);
		} else if(SearchFilter.PARENT_PRODUCT_TYPE_CODE.equals(filterName)){
			relatedInventoryParent = new RelatedInventoryFilterSO();
			relatedInventoryParent.setProductTypeCode(filterValue);
		} else if(SearchFilter.PARENT_SERVICE_ID.equals(filterName)){
			relatedInventoryParent = new RelatedInventoryFilterSO();
			relatedInventoryParent.setServiceId(filterValue);
		}
		if(relatedInventoryParent != null){
			inventoryFilterSO.setParentInventory(relatedInventoryParent);
		}
		
		RelatedInventoryFilterSO relatedInventoryChild = null;
		if(SearchFilter.CHILD_INVENTORY_ID.equals(filterName)){
			relatedInventoryChild = new RelatedInventoryFilterSO();
			relatedInventoryChild.setInventoryId(Long.parseLong(filterValue));
		} else if(SearchFilter.CHILD_INVENTORY_TYPE_CODE.equals(filterName)){
			relatedInventoryChild = new RelatedInventoryFilterSO();
			relatedInventoryChild.setInventoryTypeCode(filterValue);
		} else if(SearchFilter.CHILD_PRODUCT_TYPE_CODE.equals(filterName)){
			relatedInventoryChild = new RelatedInventoryFilterSO();
			relatedInventoryChild.setProductTypeCode(filterValue);
		} else if(SearchFilter.CHILD_SERVICE_ID.equals(filterName)){
			relatedInventoryChild = new RelatedInventoryFilterSO();
			relatedInventoryChild.setServiceId(filterValue);
		}
		if(relatedInventoryChild != null){
			inventoryFilterSO.setChildInventory(relatedInventoryChild);
		}
		
		RelatedInventoryFilterSO relatedInventory = null;
		if(SearchFilter.RELATED_INVENTORY_INVENTORY_ID.equals(filterName)){
			relatedInventory = new RelatedInventoryFilterSO();
			relatedInventory.setInventoryId(Long.parseLong(filterValue));
		} else if(SearchFilter.RELATED_INVENTORY_INVENTORY_TYPE_CODE.equals(filterName)){
			relatedInventory = new RelatedInventoryFilterSO();
			relatedInventory.setInventoryTypeCode(filterValue);
		} else if(SearchFilter.RELATED_INVENTORY_PRODUCT_TYPE_CODE.equals(filterName)){
			relatedInventory = new RelatedInventoryFilterSO();
			relatedInventory.setProductTypeCode(filterValue);
		} else if(SearchFilter.RELATED_INVENTORY_SERVICE_ID.equals(filterName)){
			relatedInventory = new RelatedInventoryFilterSO();
			relatedInventory.setServiceId(filterValue);
		}
		if(relatedInventory != null){
			InventoryLocationFilterSO locationFilterSO = new InventoryLocationFilterSO();
			locationFilterSO.setRelatedFilter(relatedInventory);
			inventoryFilterSO.setLocation(locationFilterSO);
		}
		
		//newly added						  
		if(SearchFilter.ACCOUNT_SYSTEM_CODE.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setAccountSystemCode(filterValue);
		}else if (SearchFilter.SERVICE_NAME.equalsIgnoreCase(filterName)) {
			inventoryFilterSO.setServiceName(filterValue);
		}else if (SearchFilter.APPLIANCE_NAME.equalsIgnoreCase(filterName)) {
			inventoryFilterSO.setApplianceName(filterValue);
		}else if(SearchFilter.SERVICE_ALIAS_NAME.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setServiceAliasName(filterValue);
		}else if(SearchFilter.PRODUCT_ACCOUNT_ID.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setProductAccountId(filterValue);
		}else if(SearchFilter.ACCOUNT_ALIAS_NAME.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setAccountAliasName(filterValue);
		}else if(SearchFilter.ACCOUNT_ID.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setAccountId(filterValue);	
		}else if(SearchFilter.SERVICE_ID.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setServiceId(filterValue);
		}else if(SearchFilter.PRODUCT_TYPE_CODE.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setProductTypeCode(filterValue);
		}else if(SearchFilter.PRODUCT_FAMILY_CODE.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setProductFamilyCode(filterValue);
		}else if(SearchFilter.PRODUCT_FAMILY_NAME.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setProductFamilyName(filterValue);
		}else if(SearchFilter.SERVICE_TYPE_CODE.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setServiceTypeCode(filterValue);
		}else if(SearchFilter.SERVICE_CATEGORY_ID.equalsIgnoreCase(filterName) && VStringUtils.convertStringToLong(filterValue) != 0){
			inventoryFilterSO.setServiceCategoryId(VStringUtils.convertStringToLong(filterValue));
		}else if(SearchFilter.SERVICE_ELEMENT_ID.equalsIgnoreCase(filterName) && VStringUtils.convertStringToLong(filterValue) != 0){
			inventoryFilterSO.setServiceElementId(VStringUtils.convertStringToLong(filterValue));
		}else if(SearchFilter.CALLPLAN_DESC.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setCallPlanDesc(filterValue);
		}else if(SearchFilter.DIVERSITY_INDICATOR.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setDiversityIndicator(Boolean.parseBoolean(filterValue));
		}else if(SearchFilter.QOS_INDICATOR.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setQosIndicator(Boolean.parseBoolean(filterValue));
		}else if (SearchFilter.INVENTORY_ID.equalsIgnoreCase(filterName)) {
			inventoryFilterSO.setInventoryId(Long.parseLong(filterValue));
		}else if (SearchFilter.INVENTORY_TYPE_CODE.equalsIgnoreCase(filterName)) {
			inventoryFilterSO.setInventoryTypeCode(filterValue);
		}else if(SearchFilter.SHARED_CALL_PLAN_CODE.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setSharedCallPlanCode(filterValue);
		}else if(SearchFilter.TERMINATIONS.equalsIgnoreCase(filterName)){
			inventoryFilterSO.setDnisNumber(filterValue);
			inventoryFilterSO.setRingToNumber(filterValue);
			inventoryFilterSO.setTrunkGroupName(filterValue);
		}else if (SearchFilter.TOLLFREE_DESC.equalsIgnoreCase(filterName)) {
		  inventoryFilterSO.setServiceAliasName(filterValue);
	  	}else if(SearchFilter.CONTRACT_TERMINATION_DATE.equalsIgnoreCase(filterName) && operator.equals("GE")){
			XMLGregorianCalendar contractTerminationDate = VStringUtils.getStartOfDay("MM/dd/yyyy", filterValue);
			inventoryFilterSO.setContractTerminationDate(contractTerminationDate);
		}else if(SearchFilter.CONTRACT_TERMINATION_DATE.equalsIgnoreCase(filterName) && operator.equals("LE")){
			XMLGregorianCalendar contractTerminationDate = VStringUtils.getEndOfDay("MM/dd/yyyy", filterValue);
			inventoryFilterSO.setContractTerminationDate(contractTerminationDate);
		}else if(SearchFilter.INSTALLATION_DATE.equalsIgnoreCase(filterName) && operator.equals("GE")){
			XMLGregorianCalendar installDate = VStringUtils.getStartOfDay("MM/dd/yyyy", filterValue);
			inventoryFilterSO.setInstallationDate(installDate);
		}else if(SearchFilter.INSTALLATION_DATE.equalsIgnoreCase(filterName) && operator.equals("LE")){
			XMLGregorianCalendar installDate = VStringUtils.getEndOfDay("MM/dd/yyyy", filterValue);
			inventoryFilterSO.setInstallationDate(installDate);
		}
		if(SearchFilter.CUG_ID.equalsIgnoreCase(filterName) || SearchFilter.CUG_NAME.equalsIgnoreCase(filterName) || SearchFilter.CUG_NETWORK_NAME.equalsIgnoreCase(filterName)
				|| SearchFilter.CUG_ALIAS_NAME.equalsIgnoreCase(filterName)){
			CugFilterSO cfs = new CugFilterSO();
			if (SearchFilter.CUG_ID.equalsIgnoreCase(filterName)) {
				cfs.setCugId(filterValue);
			}else if (SearchFilter.CUG_NAME.equalsIgnoreCase(filterName)) {
				cfs.setCugName(filterValue);
			}else if (SearchFilter.CUG_NETWORK_NAME.equalsIgnoreCase(filterName)) {
				cfs.setNetworkName(filterValue);
			}else if (SearchFilter.CUG_ALIAS_NAME.equalsIgnoreCase(filterName)) {
				cfs.setCugAliasName(filterValue);
			}
			inventoryFilterSO.setCugFilter(cfs);
		}
		
		if(SearchFilter.EVC_MEMBER_ID.equalsIgnoreCase(filterName) || SearchFilter.EVC_ID.equalsIgnoreCase(filterName) ||
				 SearchFilter.EVC_MEMBER_ALIAS_NAME.equalsIgnoreCase(filterName)|| SearchFilter.EVC_NAME.equalsIgnoreCase(filterName)){
			EvcMemberFilterSO evcfs = new EvcMemberFilterSO();
			if(SearchFilter.EVC_MEMBER_ID.equalsIgnoreCase(filterName)){
				evcfs.setEvcMemberId(filterValue);
			}else if(SearchFilter.EVC_ID.equalsIgnoreCase(filterName)){
				evcfs.setEvcId(filterValue);
			}else if(SearchFilter.EVC_MEMBER_ALIAS_NAME.equalsIgnoreCase(filterName)){
				evcfs.setEvcMemberAliasName(filterValue);
			}else if(SearchFilter.EVC_NAME.equalsIgnoreCase(filterName)){
				evcfs.setEvcName(filterValue);
			}
			inventoryFilterSO.setEvcMemberFilter(evcfs);
		}
		
		return inventoryFilterSO;
		
	}
	
	/*
	 * jxredd5: A helper method to determine if the InventoryFilterSO is not empty after all the mapping.
	 * Can happen, if the filter field is not something that's supported by the InventoryFilterSO like UserId,
	 * which we introduced for our internal purposes.
	 * Uses reflection to check the values of each of the field and if any field value is not null, it returns 
	 * saying that the pojo is not empty
	 */
	protected Boolean isInventoryFilterSOEmpty(InventoryFilterSO inventoryFilterSO){
		Boolean isEmpty = true;
		try{
			if(inventoryFilterSO!=null && inventoryFilterSO.getClass()!=null){
				for(Field field : inventoryFilterSO.getClass().getDeclaredFields()){
					if(field!=null){
						field.setAccessible(true);
						if(field.get(inventoryFilterSO) != null){
							isEmpty = false;
							return isEmpty;
						}
					}
				}
			}
		}catch(Exception e){
			log.error("Error determining the empty InventoryFilterSO",e);
		}
		return isEmpty;
	}

	/**
	 * @param productTypeCode
	 * @param searchFilter
	 * @return
	 * This method is to instantiate a product specific filter so based on the productTypeCode and 
	 * map any that filter specific fields and return
	 */
	protected InventoryFilterSO mapProductSpecificFilterSO(String productTypeCode, SearchFilter searchFilter){
		if(productTypeCode == null || productTypeCode.isEmpty())
			return null;
		if(productTypeCode.equals(ProductTypeCode._8XX.value())){
			return maptollfreeSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.DEDLD.value())){
			return mapDedLdSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.ATM.value())){
			return new AtmFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.DIA.value())){
			return new DiaFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.DPL.value())){
			return new DplFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.EPL.value())){
			return new EplFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.DWH.value())){
			return new DwhFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.ELINE.value())){
			return mapElineSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.FRAME.value())){
			return new FrameFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.NM.value())){
			return mapNmSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.IPL.value())){
			return new IplFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.IQNET.value())){
			return mapIqSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.LD.value())){
			return new LdFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.MOE.value())){
			return mapMoeSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.MS.value())){
			return new MsFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.NBS.value())){
			return mapNbsSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.OWS.value())){
			return new OwsFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.PL.value())){
			return new PlFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.QROUTE.value())){
			return new QrouteFilterSO();
		}else if(productTypeCode.equals(ProductTypeCode.VOIP.value())){
			return mapVoipSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.ESP.value())){
			return mapEspSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.LSS.value())){
			return new LsFilterSO();	
		}else if(productTypeCode.equals(ProductTypeCode.CE.value())){
			return mapCeSpecificFilter(searchFilter);
		}else if(productTypeCode.equals(ProductTypeCode.MFWVPN.value())){
			return mapMfwVpnSpecificFilter(searchFilter);
		}else{
			return new InventoryFilterSO();
		}
	}
	
	protected InventoryFilterSO mapCeSpecificFilter(SearchFilter searchFilter){
		CeFilterSO cef = new CeFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			  if (SearchFilter.BANDWIDTH.equalsIgnoreCase(filterName)) {
				  cef.setBandwidth(filterValue);
			  }
		}
		return cef;
	}
	
	protected InventoryFilterSO mapIqSpecificFilter(SearchFilter searchFilter){
		IqFilterSO iqf = new IqFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			  if (SearchFilter.BANDWIDTH.equalsIgnoreCase(filterName)) {
				  iqf.setBandwidth(filterValue);
			  }
		}
		return iqf;
	}
	
	protected InventoryFilterSO mapDedLdSpecificFilter(SearchFilter searchFilter){
		DedicatedLdFilterSO dedldcfs = new DedicatedLdFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			  if (SearchFilter.SERVICE_ID.equalsIgnoreCase(filterName)) {
				  dedldcfs.setServiceId(filterValue);
			  }else if (SearchFilter.DEDLD_SWITCHID.equalsIgnoreCase(filterName)) {
				  dedldcfs.setSwitchId(filterValue);
			  }
		}
		return dedldcfs;
	}
	
	protected InventoryFilterSO mapVoipSpecificFilter(SearchFilter searchFilter){
		VoipFilterSO voipcfs = new VoipFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			if (SearchFilter.VOIP_TENANT_ID.equalsIgnoreCase(filterName)) {
				  voipcfs.setTenantId(filterValue);
			  }
		}
		return voipcfs;
	}
	
	protected InventoryFilterSO mapElineSpecificFilter(SearchFilter searchFilter){
		ElineFilterSO elinef = new ElineFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			  if (SearchFilter.BANDWIDTH.equalsIgnoreCase(filterName)) {
				  elinef.setBandwidth(filterValue);
			  }
		}
		return elinef;
	}
	
	protected InventoryFilterSO mapEspSpecificFilter(SearchFilter searchFilter){
		EspFilterSO espFilterSO = new EspFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			if (SearchFilter.SIP_SWITCH_ID.equalsIgnoreCase(filterName)) {  
				espFilterSO.setSipSwitchId(filterValue);
			}else if (SearchFilter.TOTAL_SESSIONS.equalsIgnoreCase(filterName)) {
				espFilterSO.setTotalSessions(Long.parseLong(filterValue));
			}else if (SearchFilter.AVAILABLE_SESSIONS.equalsIgnoreCase(filterName)) {
				espFilterSO.setAvailableSessions(Long.parseLong(filterValue));
			}else if (SearchFilter.SERVICE_TYPE_CODE.equalsIgnoreCase(filterName)) {
				espFilterSO.setServiceTypeCode(filterValue);
			}else if (SearchFilter.SERVICE_NAME.equalsIgnoreCase(filterName)) {
				espFilterSO.setServiceName(filterValue);
			}
		}
		return espFilterSO;
	}
	
	protected InventoryFilterSO mapMoeSpecificFilter(SearchFilter searchFilter){
		MoeFilterSO moefltso = new MoeFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			if (SearchFilter.CUSTOM_NAME.equalsIgnoreCase(filterName)){
				moefltso.setServiceAliasName(filterValue);
			}else if(SearchFilter.BANDWIDTH.equalsIgnoreCase(filterName)){
				moefltso.setBandwidth(filterValue);
			}else if (SearchFilter.BANS_ACCOUNT_NUMBER.equalsIgnoreCase(filterName)){
				moefltso.setAccountId(filterValue);
			}else if(SearchFilter.QOS_INDICATOR.equalsIgnoreCase(filterName)){
				if(filterValue!=null){
					if(filterValue.equals("Yes")){
						moefltso.setQosIndicator(true);
					}else if (filterValue.equals("No")){
						moefltso.setQosIndicator(false);
					}	
				}
			}else if(SearchFilter.DIVERSITY_INDICATOR.equalsIgnoreCase(filterName)){
				if(filterValue!=null){
					if(filterValue.equals("Yes")){
						moefltso.setDiversityIndicator(true);
					}else if (filterValue.equals("No")){
						moefltso.setDiversityIndicator(false);
					}	
				}
			}
		}
		return moefltso;
	}
	
	protected InventoryFilterSO mapNbsSpecificFilter(SearchFilter searchFilter){
		NbsFilterSO nbsFilterSO = new NbsFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			 if (SearchFilter.PORT_TYPE.equalsIgnoreCase(filterName)) {
					nbsFilterSO.setServiceTypeCode(filterValue);
				}else if (SearchFilter.BANDWIDTH.equalsIgnoreCase(filterName)) {
					nbsFilterSO.setBandwidth(filterValue);
				}
		}
		return nbsFilterSO;
	}
	
	protected InventoryFilterSO maptollfreeSpecificFilter(SearchFilter searchFilter){
		TollFreeFilterSO tollfreeFilter = new TollFreeFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			if (SearchFilter.TOLLFREE_SHARED_CALL_PLAN_VALUE.equalsIgnoreCase(filterName)) {
				  tollfreeFilter.setSharedCallPlanValue(filterValue);
			}
		}
		return tollfreeFilter;
	}
	
	protected InventoryFilterSO mapNmSpecificFilter(SearchFilter searchFilter){
		NmFilterSO nmFilterSO = new NmFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			if(SearchFilter.REAL_IPV6.equalsIgnoreCase(filterName)){
				nmFilterSO.setRealIpV6(filterValue);
			} else if(SearchFilter.REAL_IP.equalsIgnoreCase(filterName)){
				nmFilterSO.setRealIpV6(filterValue);
			} else if(SearchFilter.MANAGED_IPV6.equalsIgnoreCase(filterName)){
				nmFilterSO.setRealIpV6(filterValue);
			} else if(SearchFilter.MANAGED_IP.equalsIgnoreCase(filterName)){
				nmFilterSO.setRealIpV6(filterValue);
			} 
		}
		return nmFilterSO;
	}
	
	protected InventoryFilterSO mapMfwVpnSpecificFilter(SearchFilter searchFilter){
		MfwVpnFilterSO mfwVpnFilterSO = new MfwVpnFilterSO();
		if(searchFilter != null){
			String filterName = searchFilter.getFilterName();
			String filterValue = searchFilter.getFilterValue();
			if (SearchFilter.COMPONENT_ID.equalsIgnoreCase(filterName)) {
				mfwVpnFilterSO.setComponentId(Long.parseLong(filterValue));
			}
			if (SearchFilter.STATUS_CODE.equalsIgnoreCase(filterName)) {
				mfwVpnFilterSO.setStatusCd(filterValue);
			}
		}
		return mfwVpnFilterSO;
	}

}