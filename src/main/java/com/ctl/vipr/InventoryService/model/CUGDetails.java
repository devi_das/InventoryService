package com.ctl.vipr.InventoryService.model;

public class CUGDetails{
	
	private static final long serialVersionUID = 1L;
	
	private String cugName;
	private Long cugId;
	private String cugAliasName;
	private String cugCustomName;
	private String lastModifiedByUser;
	private String lastModifiedDate;
	private Long customerAccountId; //cug CustomerId
	private Long enterpriseId; //cug enterpriseId
	private Long btlseid; //cug Service element Id
	private Long cugOwnerId;
	private String cugOwnerName;
	private String extranetInd;
	private String networkName;
	
	
	public String getCugName() {
		return cugName;
	}
	public void setCugName(String cugName) {
		this.cugName = cugName;
	}
	public Long getCugId() {
		return cugId;
	}
	public void setCugId(Long cugId) {
		this.cugId = cugId;
	}
	public String getCugAliasName() {
		return cugAliasName;
	}
	public void setCugAliasName(String cugAliasName) {
		this.cugAliasName = cugAliasName;
	}
	public String getCugCustomName() {
		return cugCustomName;
	}
	public void setCugCustomName(String cugCustomName) {
		this.cugCustomName = cugCustomName;
	}
	public String getLastModifiedByUser() {
		return lastModifiedByUser;
	}
	public void setLastModifiedByUser(String lastModifiedByUser) {
		this.lastModifiedByUser = lastModifiedByUser;
	}
	public String getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public Long getCustomerAccountId() {
		return customerAccountId;
	}
	public void setCustomerAccountId(Long customerAccountId) {
		this.customerAccountId = customerAccountId;
	}
	public Long getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(Long enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	public Long getBtlseid() {
		return btlseid;
	}
	public void setBtlseid(Long btlseid) {
		this.btlseid = btlseid;
	}
	public Long getCugOwnerId() {
		return cugOwnerId;
	}
	public void setCugOwnerId(Long cugOwnerId) {
		this.cugOwnerId = cugOwnerId;
	}
	public String getCugOwnerName() {
		return cugOwnerName;
	}
	public void setCugOwnerName(String cugOwnerName) {
		this.cugOwnerName = cugOwnerName;
	}
	public String getExtranetInd() {
		return extranetInd;
	}
	public void setExtranetInd(String extranetInd) {
		this.extranetInd = extranetInd;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}	

	
}
