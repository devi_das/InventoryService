package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;



public class VSortInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String sortColumn = "ServiceID";

	private String secondarySortField = "";

	private String sortDirection = "ASC";

	private String secondarySortDirection = "";
	
	private String tertiarySortField = null;
	
	private String tertiarySortDirection = null;
	
	public VSortInfo() {

	}	

	public String getTertiarySortField() {
		return tertiarySortField;
	}

	public void setTertiarySortField(String tertiarySortField) {
		this.tertiarySortField = tertiarySortField;
	}

	public String getTertiarySortDirection() {
		return tertiarySortDirection;
	}

	public void setTertiarySortDirection(String tertiarySortDirection) {
		this.tertiarySortDirection = tertiarySortDirection;
	}


	public VSortInfo(String sortString) {
		for(String sort : sortString.split("~")){
			if(sort != null && sort.length() > 0){				
				String[] k = sort.split(":");
				if(k.length ==2){
					if(k[0].equals("sortColumn")){
						this.sortColumn = k[1];
					}else if(k[0].equals("secondarySortField")){
						this.secondarySortField = k[1];
					}else if(k[0].equals("sortDirection")){
						this.sortDirection = k[1];
					}else if(k[0].equals("secondarySortDirection")){
						this.secondarySortDirection = k[1];
					}else if(k[0].equals("tertiarySortField")){
						this.tertiarySortField = k[1];
					}else if(k[0].equals("tertiarySortDirection")){
						this.tertiarySortDirection = k[1];
					}
				}				

			}
		}
	}

	public String getSortColumn() {
		return this.sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getSortDirection() {
		return this.sortDirection;
	}

	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	public String getSecondarySortDirection() {
		return secondarySortDirection;
	}

	public void setSecondarySortDirection(String secondarySortDirection) {
		this.secondarySortDirection = secondarySortDirection;
	}

	public String getSecondarySortField() {
		return secondarySortField;
	}

	public void setSecondarySortField(String secondarySortField) {
		this.secondarySortField = secondarySortField;
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("sortColumn:"+ sortColumn + "~");
		result.append("secondarySortField:"+secondarySortField + "~");
		result.append("tertiarySortField:"+tertiarySortField + "~");
		result.append("sortDirection:"+sortDirection + "~");
		result.append("secondarySortDirection:"+secondarySortDirection);
		result.append("tertiarySortDirection:"+tertiarySortDirection);

		return result.toString();
	}

}
