package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;
import java.util.List;

public class DAResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3950526593768674128L;
	
	public DAResponse(){
		
	}
	
	public DAResponse(DAResponse response){
		this.status = response.getStatus();
		this.message = response.getMessage();
		this.cachedResponse = response.isCachedResponse();
		this.responseTime = response.getResponseTime();
		this.networkResponseTime = response.getNetworkResponseTime();
	}
	
	
	public DAResponse (int status, String message) {
	    this.status = status;
	    this.message = message;
	}

	private Object data;
	private int status;
	private String message;
	private boolean cachedResponse;
	private long responseTime;
	private long networkResponseTime;
	private Object result;
	private List addlResults;
	private boolean queriedOnCache;
	
	public long getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isCachedResponse() {
		return cachedResponse;
	}
	public void setCachedResponse(boolean cachedResponse) {
		this.cachedResponse = cachedResponse;
	}

	public long getNetworkResponseTime() {
		return networkResponseTime;
	}

	public void setNetworkResponseTime(long networkResponseTime) {
		this.networkResponseTime = networkResponseTime;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public List getAddlResults() {
		return addlResults;
	}

	public void setAddlResults(List addlResults) {
		this.addlResults = addlResults;
	}

	public boolean isQueriedOnCache() {
		return queriedOnCache;
	}

	public void setQueriedOnCache(boolean queriedOnCache) {
		this.queriedOnCache = queriedOnCache;
	}
}
