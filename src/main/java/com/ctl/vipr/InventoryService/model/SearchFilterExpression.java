package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchFilterExpression implements Serializable{

	private static final long serialVersionUID = 149875234L;
	protected transient Logger log = LoggerFactory.getLogger(this.getClass());

	public ArrayList<SearchFilterExpression> expressions;
	public ArrayList<SearchFilterCondition> conditions;
	public String operator = SearchOptions.AND;

	public SearchFilterExpression() {
	}

	/*
	 * This constructor parses each expression, classifying based on the format, the expression belongs to.
	 * 
	 * 
	 * serviceId:123fsd* AND customName:Custom* AND portType:(IQ ENHANCED OR IQ INTERNET) AND bandwidth:>6 Mbps AND network:(The 01 Network OR SPRVU-10005388) 
	 * AND ((locationStreet:930 15th Street AND locationCity:Denver AND locationStateProvince:CO AND postalCode:80295) OR (locationStreet:123 Test Dr AND locationCity:Arvada))
	 */
	public SearchFilterExpression(String searchString) {
		/*
		 * This SearchFilterExpression is written for parsing the lucene type of searchstrings. Example strings specified above.
		 */
		if(searchString != null){
			expressions = new ArrayList<SearchFilterExpression>();
			conditions = new ArrayList<SearchFilterCondition>();
			HashMap<String, Integer> terms = new HashMap<String, Integer>();

			String termString = searchString;
			/*
			 * The parsing logic is a little complex, but works. We are assiging the searchString to a tempString as we are going to
			 * cut the parsed portion, so that tempString will be empty by the time, the whole string is parsed and triggers the end of
			 * parsing, which is what the while loop below does.
			 */
			while(termString.length() != 0){
				termString = termString.trim();
				if(log.isDebugEnabled()){
					log.debug(termString);
				}

				/*
				 * If the searchString has only one condition(eg., serviceId:DS1*, productTypeCode:(IQNET OR 8XX), then it goes to the else block of this if loop.
				 * If it is a expression with more than one condition, the logic takes it into this if loop
				 */
				if(termString.contains(SearchOptions.AND)){
					
					/*
					 * Inside this if loop, we basically browse through the expressions and determine what format it is, and give it a number and put the condition
					 * and the format number as key and value pair into a hashmap for further processing.
					 * FORMATS:
					 * 1 - Simple expressions [serviceId:123fsd*]
					 * 2 - one tier expressions [portType:(IQ ENHANCED OR IQ INTERNET)]
					 * 3 - complex expressions [((locationStreet:930 15th Street AND locationCity:Denver AND locationStateProvince:CO AND postalCode:80295) OR (locationStreet:123 Test Dr AND locationCity:Arvada))]
					 */

					/*
					 * The first if loop below is to handle the expressions like
					 * productTypeCode:IQNET AND serviceId:DS1*
					 * which does not have any parenthesis, but still is a expression
					 */
					if(!termString.contains("(")){
						for(String simpleExpr : termString.split(" AND ")){
							simpleExpr = simpleExpr.trim();
							terms.put(simpleExpr, 1);
						}
						termString = "";
					}else{
						/*
						 * The segregating logic is based on the POSITIONAL existence of parenthesis, AND and colon.
						 * Depending on where each of these exist, in the expression, we determine the format and also
						 * cut that particular expression.
						 */
						int andIndex = termString.indexOf(SearchOptions.AND);
						int openParenIndex = termString.indexOf("(");
						int colonIndex = termString.indexOf(":");
						if(openParenIndex > andIndex){
							String simpleExpr = termString.substring(0, andIndex);
							terms.put(simpleExpr, 1);
							termString = termString.substring(andIndex+4);
						}else if(openParenIndex < andIndex && openParenIndex > colonIndex){
							int closeParenIndex = getCloseParenIndex(termString, openParenIndex);
							String oneTierExpr = termString.substring(0,closeParenIndex);
							terms.put(oneTierExpr, 2);
							termString = termString.substring(closeParenIndex+5);
						}else if(openParenIndex < andIndex && openParenIndex < colonIndex){
							int closeParenIndex = getCloseParenIndex(termString, openParenIndex);
							String twoTierExpr = termString.substring(0, closeParenIndex);
							terms.put(twoTierExpr, 3);
							if(log.isDebugEnabled()){
								log.debug(Integer.toString(closeParenIndex));
								log.debug(Integer.toString(termString.length()));
							}
							if(termString.length() >= closeParenIndex+5)
								termString = termString.substring(closeParenIndex+5);
							else
								termString = "";
						}
					}

				}else{
					int openParenIndex = termString.indexOf("(");
					int colonIndex = termString.indexOf(":");
					if(!termString.contains("(")){
						String simpleExpr = termString;
						terms.put(simpleExpr, 1);
						break;
					}else if(termString.contains("(") && openParenIndex > colonIndex){

						int closeParenIndex = getCloseParenIndex(termString, openParenIndex);
						String oneTierExpr = termString.substring(0,closeParenIndex);
						terms.put(oneTierExpr, 2);
						break;
					}else if(termString.contains("(") && openParenIndex < colonIndex){
						int closeParenIndex = getCloseParenIndex(termString, openParenIndex);
						String twoTierExpr = termString.substring(0, closeParenIndex);
						terms.put(twoTierExpr, 3);
						break;
					}
				}

			}



			if(terms.keySet().size() == 1){
				String simpleExpr = "";
				int format = 0;
				for(String key : terms.keySet()){
					simpleExpr = key;
					format = terms.get(key);
				}
				if(format == 1){
					conditions.add(new SearchFilterCondition(simpleExpr));
				}else{
					expressions.add(mapExpression(simpleExpr, format));
				}
			}else{
				for(String inputExpression : terms.keySet()){
					expressions.add(mapExpression(inputExpression, terms.get(inputExpression)));
				}
			}
		}
	}



	private SearchFilterExpression mapExpression(String filterExpression, Integer queryFormat){
		log.debug(filterExpression);
		SearchFilterExpression expression = new SearchFilterExpression();
		expression.setConditions(new ArrayList<SearchFilterCondition>());

		if(log.isDebugEnabled()){
			log.debug("query format : "+queryFormat);
		}


		/*
		 * For Format1 Term:value, add it to the conditions forming a searchFilterCondition using it.
		 */
		if(queryFormat == 1 || queryFormat == 4){
			expression.addCondition(new SearchFilterCondition(filterExpression));
		}
		/*
		 * For Format2 Term:(value1 OR value2), forming
		 * filterConditions, add it to conditions array.
		 */
		else if(queryFormat == 2 ){
			expression.setOperator(SearchOptions.OR);
			String filterName = filterExpression.split(SearchFilterCondition.getOperator(filterExpression))[0];
			//trimming the brackets
			String filterValues = filterExpression.split(SearchFilterCondition.getOperator(filterExpression))[1].replace("(", "").replace(")", "");
			for(String filterValue : filterValues.split(SearchOptions.OR)){
				expression.addCondition(new SearchFilterCondition(SearchFilterCondition.getConditionalOperator(filterExpression), new SearchFilter(filterName, filterValue.trim())));
			}
		}
		/*
		 * For Format 3 : (Term1:value1 AND Term2:value2) OR (Term1:value3 AND Term2:value4)
		 * 		((Term1:(value1 OR value2)) OR (Term2:(value3 OR value4)) OR (Term3:(value5 or value6)))
		 *  
		 */
		else if(queryFormat == 3){
			expression.setOperator(SearchOptions.OR);
			if(log.isDebugEnabled()){
				log.debug("format 3 expression received : "+filterExpression);
			}
			
			filterExpression = filterExpression.trim();
			filterExpression = filterExpression.substring(1, filterExpression.length()-1);
			
			if(filterExpression.contains(SearchOptions.AND)){
				
				//This will handle the expression in the format:(location.locationTypeCode:A AND ((location.stateProvinceCode:* AND location.postalCode:*) OR (location.addressLine1:* AND location.city:*)))
				if(filterExpression.indexOf(SearchOptions.AND) < filterExpression.indexOf("(") || filterExpression.lastIndexOf(SearchOptions.AND)>filterExpression.lastIndexOf(")")){
					expression.addExpression(new SearchFilterExpression(filterExpression));
				}else {
					/*
					 * inside here when expression is of format: (Term1:value1 AND Term2:value2) OR (Term1:value3 AND Term2:value4)
					 * Easy.. divide and conquer.. split on OR and remove the opening and closing brackets and call the SearchFilterExpression to handle
					 * the subexpressions accordingly.	
					 */
					for(String subFilterExp : filterExpression.split(SearchOptions.OR)){
						String subFilterExpression = subFilterExp.trim();
						if(log.isDebugEnabled()){
							log.debug("AND OR format expression being handled : "+subFilterExpression);
						}
						subFilterExpression = subFilterExpression.replace("(", "");
						subFilterExpression = subFilterExpression.replace(")", "");
	
	
						expression.addExpression(new SearchFilterExpression(subFilterExpression));
					}
				}

			}else{
				/*
				 * inside here, when expression is of the format: ((Term1:(value1 OR value2)) OR (Term2:(value3 OR value4)) OR (Term3:(value5 or value6)))
				 * It handles this format too: ((Term1:(value1 OR value2)) OR (Term2:(value3 OR value4)) OR (Term3:value5))
				 * Complicated logic: 
				 * 1. Splitting on ") OR (" to distinguish the ORs that may come inside the sub expressions.
				 * 2. There is a possibility that the resultant subexpressions might have missing opening or closing parenthesis. Depending on what is missed, add it.
				 * 		
				 * 3. Now remove the extreme opening and closing braces from each of the subexpressions to form a tier2 or tier1 expressions
				 * 4. Use the formed tier2 or tier1 expressions and form a subFilterExpression out of each and add it to the compound expression.
				 * 
				 * If the above logic makes your brain blow away, please dont try to look for me to give me a good kick. This was the easiest logic that 
				 * could think of. Let me know, if you can think of a better logic, i can work. But for now, this works well.
				 * 
				 */
				
				for(String subFilterExp : filterExpression.split("\\)"+SearchOptions.OR+"\\(")){
					String subFilterExpression = subFilterExp.trim();
					if(log.isDebugEnabled()){
						log.debug("OR OR expression to be formatted : "+subFilterExpression);
					}

					if(subFilterExpression.startsWith("("))
						subFilterExpression += ")";
					else if(subFilterExpression.endsWith(")"))
						subFilterExpression = "(" + subFilterExpression;


					if(subFilterExpression.startsWith("(") && subFilterExpression.endsWith(")"))
						subFilterExpression = subFilterExpression.substring(1, subFilterExpression.length()-1);

					if(log.isDebugEnabled()){
						log.debug("SubExpression formed after all the processing: "+subFilterExpression);
					}

					if(!subFilterExpression.contains("(") && !subFilterExpression.contains(")") && subFilterExpression.contains(SearchOptions.OR)){
						for(String subExp : filterExpression.split(SearchOptions.OR)){
							expression.addExpression(new SearchFilterExpression(subExp));
						}
					}else {
						expression.addExpression(new SearchFilterExpression(subFilterExpression));
					}
				}
			}


		}
		return expression;
	}

	public ArrayList<SearchFilterCondition> getConditions() {
		return conditions;
	}

	public void setConditions(ArrayList<SearchFilterCondition> conditions) {
		this.conditions = conditions;
	}

	public ArrayList<SearchFilterExpression> getExpressions() {
		return expressions;
	}

	public void setExpressions(ArrayList<SearchFilterExpression> expressions) {
		this.expressions = expressions;
	}

	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void addExpression(SearchFilterExpression sfe){
		if(this.expressions != null){
			this.expressions.add(sfe);
		}else{
			this.expressions = new ArrayList<SearchFilterExpression>();
			this.expressions.add(sfe);
		}
	}

	public void addCondition(SearchFilterCondition sfc){
		if(this.conditions != null){
			this.conditions.add(sfc);
		}else{
			this.conditions = new ArrayList<SearchFilterCondition>();
			this.conditions.add(sfc);
		}
	}

	public int getQueryFormat(String query){
		if(query == null)
			return 0;

		/*Added by Raghu Ganapam for Sep Release 2011 (ERSO) 
		Expression Value can have paranthasis ex :- eBill (Paperless). In this case getQueryFormat will return 3. which will not work for this scenario.
		To fix this added below logic.
		 */ 
		if(query.contains(")") )
		{
			String queryString= query.substring(query.indexOf('(')+1, query.indexOf(')'));

			if((queryString.contains(",") || queryString.contains(":"))){
				return 3;
			} else if (query.contains("|"))
				return 2;
			else if (query.contains(":"))
				return 1;
			else if (query.contains("#"))
				return 4;
			else
				return 0;
		}
		else if (query.contains("#"))
			return 4;
		else if (query.contains("|"))
			return 2;
		else if (query.contains(":"))
			return 1;
		else
			return 0;
	}

	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		if(this.expressions != null && this.expressions.size() > 0){
			//sb.append("^^EXPN SIZE"+this.expressions.size()+"^^");
			int count = 0;
			for(SearchFilterExpression sfe : this.expressions){
				sb.append(sfe.toString());
				if(count < this.expressions.size()-1){
					sb.append("."+this.operator+".");
				}
				count++;
			}
		}
		int count1 = 0;
		if(this.conditions != null && this.conditions.size() > 0){
			//sb.append("^^COND SIZE"+this.conditions.size()+"^^");
			for(SearchFilterCondition sfc : this.conditions){
				sb.append(sfc.toString());
				if(this.expressions == null){
					if(count1 < this.conditions.size()-1){
						sb.append("."+this.operator+".");
					}
					count1++;
				}
			}
		}
		sb.append(")");

		return sb.toString();
	}

	/*
	 * The method is to assist the seggregation, to identify the ending of two-tier and complex exprs.
	 */
	public int getCloseParenIndex(String input, int openParenIndex){
		log.debug("getCloseParenIndex: "+input+" getCloseParenIndex: "+openParenIndex);
		int closeParenIndex = openParenIndex;
		int parenCount = -1;
		for(Character c : input.substring(openParenIndex).toCharArray()){
			closeParenIndex++;
			if(c.toString().equals(")") && parenCount == 0){
				return closeParenIndex;
			}else if(c.toString().equals(")") && parenCount > 0){
				parenCount--;
			}else if(c.toString().equals("(")){
				parenCount++;
			}else{
				continue;
			}
		}
		log.debug("getCloseParenIndex: "+closeParenIndex);
		return closeParenIndex;
	}

}
