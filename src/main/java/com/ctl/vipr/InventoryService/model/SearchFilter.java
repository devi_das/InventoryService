/**
 *
 */
package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;

/**
 * @author kxbx
 *
 */
public class SearchFilter implements Serializable{
	
	//location specific filter set
	
	public static final String LOCATION_SERVICE_ADDRESS_ID = "location.serviceAddressId";
	public static final String LOCATION_ADDRESS_LINE1 = "location.addressLine1";
	public static final String LOCATION_ADDRESS_LINE2 = "location.addressLine2";
	public static final String LOCATION_ADDRESS_LINE3 = "location.addressLine3";	
	public static final String LOCATION_CITY = "location.city";
	public static final String LOCATION_POSTAL_CODE = "location.postalCode";
	public static final String LOCATION_STATE_PROVINCE = "location.stateProvinceCode";
	public static final String LOCATION_COUNTRY_CODE = "location.countryCode";
	public static final String LOCATION_ID = "location.locationId";
	public static final String LOCATION_NAME = "location.locationName";
	public static final String LOCATION_TYPE_CODE = "location.locationTypeCode";
	public static final String LOCATION_CILLI_CODE = "location.clliCode";
	public static final String LOCATION_STREET = "location.street";
	public static final String LOCATION = "location.location";
	
	public static final String UBI = "ubi";
	
	//LocationZ Specific Filtering 
		public static final String LOCATIONZ_CITY = "location.zcity";
		public static final String LOCATIONZ = "locationz";
		public static final String LOCATIONZ_POSTAL_CODE = "zpostalCode";
		public static final String LOCATIONZ_STATE_PROVINCE = "location.zstateProvince";
		public static final String LOCATIONZ_ADDRESS_LINE1 = "zaddressLine1";
		public static final String LOCATIONZ_ADDRESS_LINE2 = "zaddressLine2";
		public static final String LOCATIONZ_ADDRESS_LINE3 = "zaddressLine3";	
		public static final String LOCATIONZ_CLLI_CODE = "location.zclliCode";
		public static final String LOCATIONZ_COUNTRY_CODE = "zaddressCountryCode";
		public static final String LOCATIONZ_SERVICE_ADDRESS_ID = "zserviceAddressId";	
		public static final String LOCATIONZ_STREET = "location.zstreet";
		
	//serviceId is common to all product types
	public static final String SERVICE_ID = "serviceId";
	public static final String SERVICE_TYPE_CODE = "serviceTypeCode";
	public static final String SERVICE_CATEGORY_ID = "serviceCategoryId";
	public static final String SERVICE_ELEMENT_ID = "serviceElementId";
	public static final String CALLPLAN_DESC = "callPlanDesc";
	public static final String CONTRACT_TERMINATION_DATE = "contractTerminationDate";	
	public static final String INSTALLATION_DATE = "installationDate";
	public static final String SERVICE_ALIAS_NAME = "serviceAliasName";
	public static final String PRODUCT_ACCOUNT_ID ="productAccountId";
	public static final String SHARED_CALL_PLAN_CODE = "sharedCallPlanCode";
	public static final String TERMINATIONS = "terminations";
	public static final String INVENTORY_ID = "inventoryId";
	public static final String INVENTORY_TYPE_CODE = "inventoryTypeCode";
	//added for SDWAN
	public static final String APPLIANCE_NAME="applianceName";
	
	//Related Inventory for parent and child
	public static final String PARENT_SERVICE_ID = "parentInventory.serviceId";
	public static final String PARENT_INVENTORY_ID = "parentInventory.inventoryId";
	public static final String PARENT_INVENTORY_TYPE_CODE = "parentInventory.inventoryTypeCode";
	public static final String PARENT_PRODUCT_TYPE_CODE = "parentInventory.productTypeCode";
	
	public static final String CHILD_SERVICE_ID = "childInventory.serviceId";
	public static final String CHILD_INVENTORY_ID = "childInventory.inventoryId";
	public static final String CHILD_INVENTORY_TYPE_CODE = "childInventory.inventoryTypeCode";
	public static final String CHILD_PRODUCT_TYPE_CODE = "childInventory.productTypeCode";
	
	public static final String RELATED_INVENTORY_SERVICE_ID = "inventory.serviceId";
	public static final String RELATED_INVENTORY_INVENTORY_ID = "inventory.inventoryId";
	public static final String RELATED_INVENTORY_INVENTORY_TYPE_CODE = "inventory.inventoryTypeCode";
	public static final String RELATED_INVENTORY_PRODUCT_TYPE_CODE = "inventory.productTypeCode";
	public static final String RELATED_INVENTORY_UBI = "inventory.ubi";
	
	public static final String CATALOG_LINE_ITEM_AREA_OF_WORLD = "catalogLineItem.areaOfWorldID";
	public static final String CATALOG_LINE_ITEM_PRICING_ELEMENT_CODE = "catalogLineItem.pricingElementCode";
	public static final String SERVICE_CLASSID="catalogLineItem.serviceClassID"; 
	public static final String ABBREV_ITEM_NAME = "abbrevItemName";
	public static final String CATALOG_CASE_NUMBER= "caseNumber";
	public static final String CLIN_NUMBER = "itemNumber";
	public static final String DEVICE_CLASS= "deviceClassID";
	public static final String DEVICE_SIZE= "deviceSizeCode";
	public static final String EFFICIENCY_COMPLIANCE= "energyEfficiencyRating";
	public static final String MANUFACTURER_NMAE= "manufacturerName";
	public static final String TYPE_CODE= "typeCode";
	public static final String CATALOG_LINE_ITEM_NUMBER = "catalogLineItem.itemNumber";
	public static final String ASCENDING_ORDER = "Ascending";
	public static final String DESCENDING_ORDER = "Descending";
	public static final String INCLUDE_ENDOFLIFE = "includeEndOfLife";
	
	//DEDLD specific filter string
	public static final String DEDLD_SWITCHID = "switchId";
	
	//VOIP specific filter string
	public static final String VOIP_TENANT_ID = "tenantId";
	
	
	//IQ specific filter strings
	public static final String PORT_TYPE = "portType";
	public static final String CUSTOM_NAME = "customName";
	public static final String BANDWIDTH = "bandwidth";
	public static final String CUG_NETWORK_NAME = "networkName";
	public static final String CUG_ID = "cugId";
	public static final String CUG_NAME = "cugName";
	public static final String CUG_ALIAS_NAME = "cugAliasName";
	public static final String DEVICE_ID = "deviceId";
	public static final String DEVICE_NAME = "deviceName";
	
	//PL specific
	public static final String DIVERSITY_INDICATOR = "diversityIndicator";
	public static final String PRODUCT_FAMILY_CODE = "productFamilyCode";
	public static final String PRODUCT_TYPE_CODE = "productTypeCode";
	public static final String PRODUCT_FAMILY_NAME = "productFamilyName";
	
	//ESP Specific
	public static final String POOL_TYPE = "poolType";
	public static final String SIP_SWITCH_ID = "sipSwitchId";
	public static final String TOTAL_SESSIONS = "totalSessions";
	public static final String AVAILABLE_SESSIONS = "availableSessions";
	public static final String SERVICE_NAME = "serviceName";
	
    //MOE specific
	public static final String QOS_INDICATOR = "qosIndicator";
	public static final String BANS_ACCOUNT_NUMBER = "bansAccountNumber";
	public static final String ACCOUNT_ID = "customerAccountId";
	public static final String ACCOUNT_ALIAS_NAME="customerAccountAliasName";
	public static final String ACCOUNT_SYSTEM_CODE = "accountSystemCode";
	public static final String EVC_ID = "evcId";
	public static final String EVC_MEMBER_ID = "evcMemberId";
	public static final String EVC_MEMBER_ALIAS_NAME = "evcMemberAliasName";
	public static final String EVC_NAME = "evcName";
	
	//Tollfree specific filter strings
	public static final String TOLLFREE_NUMBER = "tollfreeNumber";
	public static final String TOLLFREE_DNIS_NUMBER = "dnisNumber";
	public static final String TOLLFREE_CUSTOM_NAME = "customName";
	public static final String TOLLFREE_TRUNKGROUP_NAME = "trunkGroupName";
	public static final String TOLLFREE_RING_TO_NUMBER = "ringToNumber";
	public static final String TOLLFREE_CALL_PLAN_DESC  = "callPlanDesc";
	public static final String TOLLFREE_DESC = "tollFreeDesc";
	public static final String TOLLFREE_SHARED_CALL_PLAN_CODE = "sharedCallPlanCode";
	public static final String TOLLFREE_SHARED_CALL_PLAN_VALUE = "sharedCallPlanValue";
	public static final String FEATURE_CODE="feature.featureCd";
	
	//	 AccountManagement specific filter Strings
	public static final String ACCONT_MGMT_INCLUDE_SUB_ACCOUNTS = "includeInvoiceSubAccts";
	public static final String ACCONT_MGMT_ACCOUNT_TYPE = "accountType";
	public static final String ACCONT_MGMT_PRODUCT_TYPE_CODE = "productTypeCode";
	public static final String ACCONT_MGMT_INCLUDE_PRODUCT_ACCOUNTS_ONLY = "includeProductAccountsOnly";
	public static final String ACCONT_MGMT_STATUS_CODE = "statusCode";
	public static final String ACCONT_MGMT_ACCOUNT_NAME ="accountName";
	public static final String ACCONT_MGMT_SUB_ACCOUNT_GROUP_ID="subAccountGroupId";
	public static final String ACCOUNTID="accountId"; 
	public static final String ACCOUNT_REGIONCODE="accountRegionCode"; 
	public static final String ACCOUNT_TYPE_CODE="accountTypeCode"; 
	public static final String EXCLUDE_NON_ENT_ABS_ACCOUNT="excludeNonEnterpriseIabsAccounts"; 
	public static final String SUB_ACCOUNT_INDICATOR="subAccountIndicator";  
	public static final String SUB_ACCOUNT_TYPE_CODE="subAccountTypeCode";
	public static final String SHELL_ACCOUNTI_D="shellAcctInd";
	public static final String TASK_ORDER_ID="taskOrderId";
	
	//NM Specific
	public static final String REAL_IPV6 = "realIpV6";
	public static final String REAL_IP = "realIp";
	public static final String MANAGED_IPV6 = "managedIpV6";
	public static final String MANAGED_IP = "managedIp";	
	
	//MFWVPN Specific
	public static final String COMPONENT_ID = "componentId";
	public static final String STATUS_CODE = "statusCode";
	
	public String filterName;
	public String filterValue; 
	
	public SearchFilter(){
		
	};
		
	public SearchFilter(String filterName, String filterValue) {
		this.filterName=filterName;
		this.filterValue=filterValue;
	}

	/**
	 * @return the filterName
	 */
	public String getFilterName() {
		return filterName;
	}

	/**
	 * @return the filterValue
	 */
	public String getFilterValue() {
		return filterValue;
	}
	
}
