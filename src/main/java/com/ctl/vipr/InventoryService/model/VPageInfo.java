package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;

import org.apache.commons.lang3.math.NumberUtils;

public class VPageInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private int blockSize=250;
	private int startIndex=0;
	private int pageNumber = -1;
	/**
	 * @return the pageNumber
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public VPageInfo()
	{
	}

	public VPageInfo(int startIndex, int blocksize)
	{
		this.startIndex = startIndex;
		this.blockSize = blocksize;

	}
	/**
	 * This Constructor accepts PageInfo String(<startIndex>^<BlockSize>) and forms SiPageInfo object with pageNumer, blockSize and startIndex Values.
	 * Example : 
	 * 			PageInfo String -> "2^5" . Here 2 is a start Index and 5 is a block size. 
	 * @param pageInfoString
	 */
	public VPageInfo(String pageInfoString)
	{
		if(!VStringUtils.isEmpty(pageInfoString)){
			String[] k = pageInfoString.split("\\^");
			if(k.length == 2 && k[0] != null && k[1] != null && NumberUtils.isNumber(k[0]) && NumberUtils.isNumber(k[1])){
				this.startIndex = Integer.parseInt(k[0]);
				this.blockSize = Integer.parseInt(k[1]);
			}
		}else{
			this.startIndex = 0;
			this.blockSize = 10000;
		}
	}

	public int getBlockSize() {
		return this.blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	public int getStartIndex() {
		return this.startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}


	public String toString() {

		StringBuffer result = new StringBuffer();
		result.append(startIndex+"^");
		result.append(blockSize);
		return result.toString();
	}
}
