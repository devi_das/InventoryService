package com.ctl.vipr.InventoryService.model;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VStringUtils extends StringUtils{
	private static Logger log = LoggerFactory.getLogger(VStringUtils.class);

	/**
	 * Checks if a string is empty - null strings, "", and "  " are considered empty.
	 *
	 * @param string A String value.
	 *
	 * @return A boolean <code>true</code> if the String is empty,
	 * otherwise <code>false</code>.
	 */
	public static boolean isEmpty(String string) {
		return (string==null || string.trim().length()==0);
	}

	/**
	 * Returns a List containing the first, middle, and last name given the name
	 * passed to it.
	 * @param name A customer name.
	 * @return A list containing the first, middle, and last name, or null.
	 */
	public static List getFirstMiddleLast(String name) {
		if (name==null) return null;
		List nameList = new ArrayList();
		String cleanFullName = cleanName(name);

		if (isEmpty(cleanFullName)) return null;

		String restOfName = cleanFullName.substring(cleanFullName.indexOf(" ")+1, cleanFullName.length());
		String middle = "", last = "";
		if (!isEmpty(restOfName) && restOfName.indexOf(" ")>=1) {
			middle = restOfName.substring(0, restOfName.indexOf(" "));
			last = restOfName.substring(restOfName.indexOf(" ")+1, restOfName.length());
		} else {
			last = restOfName;
		}
		int idx = cleanFullName.indexOf(" ");
		if(idx != -1){
			nameList.add(0, cleanFullName.substring(0, idx));
		nameList.add(1, middle);
		nameList.add(2, last);
		}else{
			nameList.add(0,cleanFullName);
		}
		return nameList;
	}

	/**
	 * Utility method that removes any prefix, suffix or other special characters
	 * from the full name off of the C+ name struct.
	 * @param name
	 * @return
	 */
	public static String cleanName(String name) {
		if (isEmpty(name)) return "";

        // remove '+', '&', and '.' if exists
        name = name.replaceAll("\\+", "");
        name = name.replaceAll("\\&", "");
        name = name.replaceAll("\\.", "");

        String outputName = "";
        List remove = Arrays.asList(new String[]{"MR", "MRS", "MS", "I", "II", "III", "IV", "JR", "SR"});
        StringTokenizer st = new StringTokenizer(name);

        while (st.hasMoreTokens()){
            String token = st.nextToken();
            if (!remove.contains(token))
                outputName = outputName + token + " ";
        }
        return outputName.trim();
    }

	public static String replace(String regExpr, String inputStr, String replaceStr) {
		if (isEmpty(regExpr) || isEmpty(inputStr)) return "";
		if (replaceStr==null) replaceStr = "";
		Pattern pattern = Pattern.compile(regExpr);
		Matcher matcher = pattern.matcher(inputStr);
		return matcher.replaceAll(replaceStr);
	}

	public static boolean getBooleanValue (String booleanString) {
		boolean boolValue = false;
        if(booleanString != null && booleanString.trim().length() != 0) {
            boolValue = Boolean.valueOf(booleanString);
        }
        return boolValue;
	}
	
	public static long getLongValue (String valueString) {
		long value = 0L;
        if(valueString != null && valueString.trim().length() != 0) {
            value = Long.parseLong(valueString);
        }
        return value;
	}

	/**
	 * Converts a String into an integer. If an exception occurs,
	 * zero is returned.
	 * @param intStr
	 * @return
	 */
   	public static int convertStringToInt(String intStr) {
   		try {
   	   		return Integer.parseInt(intStr);
   		} catch(NumberFormatException ex) {
   			return 0;
   		}
   	}
   	
	/**
	 * Converts a String into an long. If an exception occurs,
	 * zero is returned.
	 * @param intStr
	 * @return
	 */
   	public static long convertStringToLong(String intStr) {
   		try {
   	   		return Long.parseLong(intStr);
   		} catch(NumberFormatException ex) {
   			return 0;
   		}
   	}

	public static String nullToEmpty(String inputStr) {
		if(inputStr == null)
			return "";
		return inputStr.trim();
	}

	public static String subString(String strInput, int startIndex, int endIndex) {
		try{
			return strInput.substring(startIndex, endIndex);
		}catch(Exception e) {
			return "";
		}
	}
	
	public static String subStringFromEnd(String strInput, int numberOfChar) {
		try{
			return strInput.substring(strInput.length() - numberOfChar, strInput.length());
		}catch(Exception e) {
			return "";
		}
	}	

	public static String getFormattedSSN(String inputSSN) {
		if(isEmpty(inputSSN))
			return "";
		String formattedSSN = inputSSN;
		try{
			if(inputSSN.charAt(3) == '-') {
				formattedSSN = subString(inputSSN,0,3) + subString(inputSSN,4,6);
			}
			if(inputSSN.charAt(6) == '-') {
				formattedSSN += subString(inputSSN,7,inputSSN.length());
			}
		}catch(Exception e) {
		}
		return formattedSSN;
	}

	public static String prePad(String inputStr, char paddingChar, int length) {
		if(isEmpty(inputStr))
			return inputStr;
		StringBuffer outputStr = new StringBuffer(inputStr);
		try{
			if(inputStr.length() < length) {
				int padLength = length - inputStr.length();
				outputStr = new StringBuffer();
				for(int p=0; p<padLength; p++) {
					outputStr.append(paddingChar);
				}
				outputStr.append(inputStr);
			}
		}catch(Exception e) {

		}
		return outputStr.toString();
	}

	//For Velocity Formatting

	public static String pad(String padChar, int length) {
		char paddingChar = padChar.charAt(0);
		StringBuffer outputStr = new StringBuffer();
		for(int p=0; p<length; p++) {
			outputStr.append(paddingChar);
		}
		return outputStr.toString();
	}

	public static String prePad(String inputStr, String padChar, int length) {
		char paddingChar = padChar.charAt(0);
		if(isEmpty(inputStr))
			inputStr = " ";
		StringBuffer outputStr = new StringBuffer(inputStr);
		if(inputStr.length() < length) {
			int padLength = length - inputStr.length();
			outputStr = new StringBuffer();
			for(int p=0; p<padLength; p++) {
				outputStr.append(paddingChar);
			}
			outputStr.append(inputStr);
		}
		return outputStr.toString();
	}

	public static String postPad(String inputStr, String padChar, int length) {
		char paddingChar = padChar.charAt(0);
		if(isEmpty(inputStr))
			inputStr = " ";
		StringBuffer outputStr = new StringBuffer(inputStr);
		if(inputStr.length() < length) {
			int padLength = length - inputStr.length();
			for(int p=0; p<padLength; p++) {
				outputStr.append(paddingChar);
			}
		}
		return outputStr.toString();
	}

	/**
	 * Converts a string delimited by 'delim' to an ArrayList
	 * @param str
	 * @param delim
	 * @return
	 */
	public static ArrayList convertStringToList(String str, String delim) {		
		ArrayList list = new ArrayList();
		if(isEmpty(str) || isEmpty(delim)) return list;
		try {
			StringTokenizer tokenizer = new StringTokenizer(str, delim);
			while(tokenizer!=null && tokenizer.hasMoreTokens()) {
				list.add(tokenizer.nextToken());
			}			
		} catch (Exception ex) {
			log.error("Exception while trying to convert String to List.",ex);
		}
		return list;
	}
		
	/**
	 * Checks if a string only contains digits and no alpha characters.
	 * @param str
	 * @return
	 */
	public static boolean containsOnlyDigits(String str) {
		if (VStringUtils.isEmpty(str)) return false;
		for (int i=0; i<str.length(); i++) {			
		    if (!Character.isDigit(str.charAt(i))) return false;
		}
		return true;
	}	

	/**
	 * Removes all text before the character value passed in.  
	 * All text after the character is returned as the result.
	 * @param str
	 * @param character
	 * @return
	 */
	public static String subString(String str, char character) {
		if (isEmpty(str)) return "";
		String newStr = "";
		try {
			newStr = str.substring(str.indexOf(character)+1);
		} catch (Exception ex) {
			log.error("Exception trying to parse string: " + str);
		}
		return newStr;			
	}
	
	/**
	 * Incase of a String of form (value), the functions removes
	 * '(' and ')' and appends '-' to the String i.e -value.   
	 * @param str 
	 * @return A String with '-' appended to it's front if
	 * the input is of the form "(value)"
	 */
	public static String displaySign(String str){
		if (isEmpty(str)) return "";
		str = str.trim();
		if( str.startsWith("(") && str.endsWith(")")){			
			return "-" + (str.substring(1, (str.length() - 1)).trim()) ;
		}
		return str;
	}
	
	/**
	 * Returns formatted time in "MM/DD/YYYY HH:MM:SS MS" TimeZone format
	 * @return formatted time
	 */
	public static String getFormattedTime() {
		Calendar cal = Calendar.getInstance();
		return (cal.get(Calendar.MONTH) +1) + "/" + cal.get(Calendar.DATE) + "/" + cal.get(Calendar.YEAR)
				+ " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE)
				+ ":" + cal.get(Calendar.SECOND)+ " " + cal.get(Calendar.MILLISECOND) 
				+ " "+ cal.getTimeZone().getDisplayName();
	}
	
	/**
	 * Returns formatted time in "MM/DD/YYYY HH:MM:SS MS" TimeZone format
	 * @return formatted time
	 */
	public static String getFormattedTime(String milliSeconds) {
		Calendar cal = Calendar.getInstance();
		String formattedTime = "";
		try{
			cal.setTimeInMillis(Long.parseLong(milliSeconds));
			formattedTime = (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.DATE) + "/" + cal.get(Calendar.YEAR)
				+ " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE)
				+ ":" + cal.get(Calendar.SECOND)+ " " + cal.get(Calendar.MILLISECOND) 
				+ " "+ cal.getTimeZone().getDisplayName();
		} catch(Exception e) {
			log.error("Number format exception ",e);
		}
		return formattedTime;
	}
	
	/**
	 * Uses System.getProperty("line.separator") to append a line break at the end of each line
	 * @param data
	 */
	public static void appendln(StringBuffer data, String line) {
		data.append(line);
		data.append(System.getProperty("line.separator"));
	}
	
	public static String getFormattedTN(String tn) {
		String formattedTn = tn;
		try {
			formattedTn = tn.substring(0,3) + "-" + tn.substring(3,6) + "-" + tn.substring(6); 
		} catch(Exception e) {
			log.error("Exception formatting TN");
		}
		return formattedTn;
	}
	
	public static Date getDateFromString(String strDate, String format) {
		Date date = new Date();
		try { 
			SimpleDateFormat sdf = new SimpleDateFormat (format);
			TimeZone toTimeZone = TimeZone.getTimeZone("GMT");
			sdf.setTimeZone(toTimeZone);
			date = sdf.parse(strDate);
		} catch(Exception e) {
			log.error("Exception converting date from string ",e);
		}
		return date;
	}
	
	public static Date getLocalDateFromString(String strDate, String format) {
		Date date = new Date();
		try { 
			SimpleDateFormat sdf = new SimpleDateFormat (format);
			date = sdf.parse(strDate);
		} catch(Exception e) {
			log.error("Exception converting date from string ",e);
		}
		return date;
	}
	
	public static Calendar getCalendarDateFromString(String strDate, String time) {		
		Calendar cal = Calendar.getInstance();
		String RCE_DATE_FORMAT = "MM-dd-yy";
		String RCE_DATE_TIME_FORMAT = "MM-dd-yy hhmmaaa";
		try { 
			if(!isEmpty(time)) {
				SimpleDateFormat sdf = new SimpleDateFormat (RCE_DATE_TIME_FORMAT);
				Date date = sdf.parse(strDate + " " + time + "M");
				cal.setTime(date);
				cal.add(Calendar.MONTH, 1);
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat (RCE_DATE_FORMAT);
				Date date = sdf.parse(strDate);
				cal.setTime(date);
				cal.add(Calendar.MONTH, 1);
			}			
		} catch(Exception e) {
			log.error("Exception converting String to Calendar ",e);
			return null;
		}
		return cal;
	}
	
	public static Calendar getCalendarObjectFromString(String strDate) {		
		Calendar cal = Calendar.getInstance();
		try { 
			DateFormat formatter ; 
	        Date date ; 
	        formatter = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
	        date = formatter.parse(strDate);
	        cal.setTime(date);
		} catch(Exception e) {
			log.error("Exception converting String to Calendar object ",e);
			return null;
		}
		return cal;
	}
	
	public static String getStringFromDate(Calendar calendar) {		
		String dateStr = "";
		String DATE_FORMAT = "MM-dd-yy";
		try { 
			java.text.SimpleDateFormat sdf = 
				   new java.text.SimpleDateFormat(DATE_FORMAT);
			if(calendar != null) {
				//Needs Clone, otherwise original calendar obj MONTH data will be changed
				// through reference
				Calendar cal = (Calendar) calendar.clone();
				//MONTH is zero based, so subtract 1
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) -1);
				dateStr = sdf.format(cal.getTime());
			}
			
		} catch(Exception e) {
			log.error("Exception converting Date to String ",e);
			return null;
		}
		return dateStr;
	}
	
	public static String getStringFromDate(Calendar calendar, String format) {
        String dateStr = "";
        try {
            java.text.SimpleDateFormat sdf =
                   new java.text.SimpleDateFormat(format);
            if(calendar != null) {
                //Needs Clone, otherwise original calendar obj MONTH data will be changed
                // through reference
                Calendar cal = (Calendar) calendar.clone();
                 sdf.setTimeZone(cal.getTimeZone());
                dateStr = sdf.format(cal.getTime());
            }

        } catch(Exception e) {
            log.error("Exception converting Date to String ",e);
            return null;
        }
        return dateStr;
    }
	
	public static String getStringFromDate(Date date, String format) {
        String dateStr = "";
        try {
            java.text.SimpleDateFormat sdf =
                   new java.text.SimpleDateFormat(format);
            TimeZone toTimeZone = TimeZone.getTimeZone("GMT");
			sdf.setTimeZone(toTimeZone);
            if(date != null) {
                //Needs Clone, otherwise original calendar obj MONTH data will be changed
                // through reference
                dateStr = sdf.format(date);
            }

        } catch(Exception e) {
            log.error("Exception converting Date to String ",e);
            return null;
        }
        return dateStr;
    }
   /**
     *  Get Date from Text using the date format specified
     *  if any exception caught return null
     *
     * @param strDate
     * @param format
     * @return Date
     */
    public static Date getDateObjFromString(String strDate, String format) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat (format);
            if(!isEmpty(strDate))
            	date = sdf.parse(strDate);
        } catch(Exception e) {
        	log.error("Can't parse input : "+strDate+" to format : "+format);
            return null;
        }
        return date;
    }
	
	public static ArrayList<String> splitString(String data, int length) {
		ArrayList<String> splitLst = new ArrayList<String>();
		try {
			for (int i = 0; i < data.length(); i = i + length)
			{
				if (i + length < data.length())
				{
					splitLst.add(data.substring(i, i + length));
				}
				else
				{
					splitLst.add(data.substring(i));
				}
			}
		} catch(Exception e) {
			log.error("Exception splitting string ",e);
		}
		return splitLst;
	}
	
    
    /**
     * Trims the input String
     * @param input
     * @return Returns a cleaned String or null
     */
    public static String cleanString(String input){
        if(isEmpty(input)){
            return null;
        }
        return input.trim();
    }
    
    /**
     * Trims the input circuitId and gives the CircuirId number alone
     * @param circuitId
     * @return
     */
     public static String getCircuitIdNumber(String circuitId){
    	String cktIdNumber = "";
    	if (!isEmpty(circuitId) && circuitId.indexOf("-") > -1){
    		circuitId = circuitId.split("-")[1];
    		if (circuitId.indexOf(".") > -1){
    			circuitId = circuitId.split("\\.")[0];    			
    		}    		
		try{
			Integer.parseInt(circuitId);
			cktIdNumber = circuitId;
		}
		catch(Exception e){
			char[] dst =	circuitId.toCharArray();
			for(int i = 0 ; i < dst.length ; i++){				
				if (!Character.isDigit(dst[i])){					
					continue;
				}				
				cktIdNumber = cktIdNumber.concat(String.valueOf(dst[i]));
			}
		}
    	}
    	return cktIdNumber;    	
    }
    /**
     * This method removes the preceeding and trailing char c from the String s. Char c can occur continously.
     * This method removes all of them.
     * eg., String s like "---Erate-Program--" or "-Erate-Program" or "Erate-Program---" with the char c as "-", 
     * when passed as parameters to this method results in "Erate-Program" 
     * @param s
     * @param c
     * @return
     */
    public static String removeCharAtStartAndEnd(String s, char c){
    	String trimString = "";
    	if(s != null){
    		StringBuffer sb = new StringBuffer(VStringUtils.removeCharAtStart(s,c));
    		String revTrimString = VStringUtils.removeCharAtStart(sb.reverse().toString(),c);		
    		sb = new StringBuffer(revTrimString);
    		trimString = sb.reverse().toString();
    	}
		return trimString;
    }
    /**
     * This method removes the preceeding char c from the String s. Char c can occur continously.
     * This method removes all of them.
     * eg., String s like "---Erate-Program--" or "-Erate-Program--" or "Erate-Program--" with the char c as "-", 
     * when passed as parameters to this method results in "Erate-Program--" 
     * @param s
     * @param c
     * @return
     */
    public static String removeCharAtStart(String s, char c){
    	String s1 = s;
    	if(s != null){
    		for(int i=0; i<s.length(); i++){
    			if(s.charAt(i) == c){
    				s1 = s.substring(i+1);
    			}else{
    				break;
    			}
    		}
    	}
    	return s1;
    }    
	/*
	 * The toArgsString method forms the argument string for logging. If the argument is of primitive type or 
	 * a String object or a Date object or a calendar object, the value is used, else the simple name of the
	 * object is used. All the arguments are delimited by / 
	 */
	public static String toArgsString(Object[] args){
		StringBuilder ar = new StringBuilder();
		String ARGS_DELIMITER = "/";
		if (args != null && args.length >= 1) {
			for(Object arg : args){
				if(arg instanceof Object){
					if(arg instanceof String){
						ar.append(arg);
						ar.append(ARGS_DELIMITER);
					}else if(arg instanceof Date){
						Date date = (Date)arg;
						ar.append(date.getTime());
						ar.append(ARGS_DELIMITER);
					}else if(arg instanceof Calendar){
						Calendar cal = (Calendar)arg;
						ar.append(cal.getTimeInMillis());
						ar.append(ARGS_DELIMITER);
					}else{
						ar.append(arg.getClass().getSimpleName());
						ar.append(ARGS_DELIMITER);
					}
				}else{
					ar.append(arg);
					ar.append(ARGS_DELIMITER);
				}
			}
		}
	   return ar.toString();
	}
		
	public static String removeChar(String s, char c) {
		String r = "";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}
		return r;
	}
	
	/**
	 * This is used for testing locally
	 * @param filePath
	 * @return
	 * @throws Exception 
	 */
	
	public String readFileAsString(String filePath) throws Exception {
	    byte[] buffer = new byte[(int) new File(filePath).length()];
	    BufferedInputStream f = null;
	    try {
	        f = new BufferedInputStream(new FileInputStream(filePath));
	        f.read(buffer);
	    }catch (Exception e) {
	    	if(log.isDebugEnabled())
	    		log.debug(e.getMessage());
	    	throw e;
		} finally {
	        if (f != null) try { f.close(); } catch (IOException ignored) { }
	    }
	    return new String(buffer);
	}
	
	/**
     *  This Method is to convert the Date object based on the targetTimeZone given.
     *  it returns the Date in String format.
     * @param targetTimeZone
     * @param d
     * @return String
     */
	public static String convertDateByTimeZone(String targetTimeZone, Date d) {		
		String strDate="";
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm aaa");
		TimeZone toTimeZone = TimeZone.getTimeZone(targetTimeZone);
		formatter.setTimeZone(toTimeZone);
		strDate = formatter.format(d);
		return strDate;
	}



	/**
     *  Get Date from Text using the date format dd MMM yyyy HH:mm:ss z
     *  if any exception caught return null
     *
     * @param strDate
     * @return Date
     */
    public static Date getDateObjFromString(String strDate) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy hh:mm aaa");
            if(!isEmpty(strDate))
            	date = sdf.parse(strDate);
        } catch(Exception e) {
        	log.error("Can't parse input : "+strDate+" to format :");
            return null;
        }
        return date;
    }
    
    
	public static Calendar getCalendarObjectFromString(String dateFormat, String strDate) {		
		Calendar cal = Calendar.getInstance();
		try { 
			DateFormat formatter ; 
	        Date date ; 
	        formatter = new SimpleDateFormat(dateFormat);
	        date = formatter.parse(strDate);
	        cal.setTime(date);
		} catch(Exception e) {
			log.error("Exception converting String to Calendar object ",e);
			return null;
		}
		return cal;
	}
	public static XMLGregorianCalendar getStartOfDay(String dateFormat, String strDate) {
		XMLGregorianCalendar xmlGreCal = null;
				GregorianCalendar greCal  = new GregorianCalendar();
		try { 
			DateFormat formatter ; 
	        Date date ; 
	        formatter = new SimpleDateFormat(dateFormat);
	        date = formatter.parse(strDate);
	        greCal.setTime(date);
	        xmlGreCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(greCal);
		} catch(Exception e) {
			log.error("Exception converting String to Calendar object ",e);
			return null;
		}
		return xmlGreCal;
	}
	
	public static XMLGregorianCalendar getEndOfDay(String dateFormat, String strDate) {
		XMLGregorianCalendar xmlGreCal = null;
				GregorianCalendar greCal  = new GregorianCalendar();
		try { 
			DateFormat formatter ; 
	        Date date ;
	        formatter = new SimpleDateFormat(dateFormat);
	        date = formatter.parse(strDate);
	        greCal.setTime(date);
	        greCal.set(GregorianCalendar.HOUR_OF_DAY, 23);
	        greCal.set(GregorianCalendar.MINUTE, 59);
	        greCal.set(GregorianCalendar.SECOND, 59);
	        greCal.set(GregorianCalendar.MILLISECOND, 999);
	        xmlGreCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(greCal);
		} catch(Exception e) {
			log.error("Exception converting String to Calendar object ",e);
			return null;
		}
		return xmlGreCal;
	}
	
	
	// Below are the methods used mainly for getNTMMetaData Service 
	
//	public class StringComparator implements Comparator<String>, Serializable {
//		private static final long serialVersionUID = 2790486368887934707L;
//
//		public int compare(String s1, String s2) {
//			if (s1 == null && s2 == null)
//				return 0;
//			else if (s1 == null)
//				return -1;
//			else if (s2 == null)
//				return 1;
//			return s1.compareTo(s2);
//		}
//	}
	
	public static String generateHashMapKey(String keyString) {
		if (StringUtils.isBlank(keyString)) {
			//log.error("Input KeyString is blank... So returning NULL");
			return null;
		}
		String key;
		key = StringUtils.upperCase(StringUtils.deleteWhitespace(keyString));
		return key;
	}
	
	public static String generateHashMapKey(String keyString, boolean removeWhiteSpace) {
		if (StringUtils.isBlank(keyString)) {
			log.error("Input KeyString is blank... So returning NULL");
			return null;
		}
		String key;
		if (removeWhiteSpace)
			key = StringUtils.upperCase(StringUtils.deleteWhitespace(keyString));
		else
			key = StringUtils.upperCase(StringUtils.trim(keyString));
		log.debug("Generated key string thats getting returned is :: " + key
				+ " and removeWhiteSpace was set to :: " + removeWhiteSpace);
		return key;
	}
	
	public static String getStringFormatOfBeanObject(Object obj){		
		if(obj == null){
			log.error("Object to be converted in String format is null. So returning back");
			return "";
		}
		StringBuffer result = new StringBuffer();
		String newLine = System.getProperty("line.separator");

		result.append(obj.getClass().getName());
		result.append(" Object {");
		result.append(newLine);

		Method[] mthods = obj.getClass().getMethods();

		for (int i = 0; i < mthods.length; i++) {
			Method method = mthods[i];
	
			if (isGetter(method)) {
				result.append("  ");
				try {
					result.append(method.getName());
					result.append(" : ");
					result.append(method.invoke(obj, null));
				} catch (Exception ex) {
					log.error("There was an Exception while invoking refection methods while formating the object in form of String. MethodName is :: "+method.getName()+"\n"+ ex.getMessage());
				}
				result.append(newLine);
			}
		}
		result.append("}");		
		return result.toString();
	}
	/**
	 * Check whether this is a getter method.
	 * 1.) Name will start with get
	 * 2.) There wont be any parameters
	 * 3.) Return type should not be void
	 * 
	 * @param method
	 * @return
	 */ 
	private static boolean isGetter(Method method) {
		if (!method.getName().startsWith("get"))
			return false;
		if (method.getParameterTypes().length != 0)
			return false;
		if (void.class.equals(method.getReturnType()))
			return false;
		return true;
	}

	/**
	 * Below method will help in checking the given data string is of Long value.
	 * if blankFlag is set to 'true', method will return ture if dataString is
	 * empty or null, otherwise returns false  
	 */
	public static boolean isLong(String dataStr, boolean blankFlag){
		boolean isLong = false;
		if(isBlank(dataStr) && blankFlag)
			isLong = true;
		else{
			try{
				Long.parseLong((dataStr == null)? null: dataStr.trim());
				isLong = true; //Did not throw, must be a number
			}catch(NumberFormatException err){
				isLong = false; //Threw, So is not a number
			}
		}
		return isLong;
	}
	
	public static XMLGregorianCalendar convertStringToXMLGregorianCalendar(String date){
		XMLGregorianCalendar  xmlCal=null;
		DateFormat format = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss'GMT'" );
		 TimeZone toTimeZone = TimeZone.getTimeZone("GMT");
		 try{
		 format.setTimeZone(toTimeZone);
		 Date dat = format.parse(date);
		 GregorianCalendar   cal = new GregorianCalendar();
		 cal.setTime(dat);
		 xmlCal=DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		
	}catch(Exception e){
		return null;
	}
		return xmlCal;
	}
	
	public static boolean isAlphaNumericAt(String field) {

        String valid = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_ ";

        for (int i = 0; i < field.length(); i++) {
            if (valid.indexOf(field.charAt(i)) < 0) {
                return false;
            }
        }
        return true;
    }
	public static boolean isDate(String field) {
        DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        sdf.setLenient(false);
        try {
            sdf.parse(field);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    public static boolean isFutureDate(String field) {
        Date d1 = null;
        DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        sdf.setLenient(false);
        try {
            d1 = sdf.parse(field);
        } catch (Exception e) {
            return false;
        }

        if (d1.compareTo(new java.util.Date()) < 1) {
            return false;
        }
        return true;
    }
	
   
}
