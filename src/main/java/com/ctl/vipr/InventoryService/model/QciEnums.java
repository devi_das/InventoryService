package com.ctl.vipr.InventoryService.model;

public class QciEnums {

	public enum ProductTypeCode {
		IQNET("IQNET"), NBS("NBS"), PL("PL"), EPL("EPL"), DPL("DPL"), IPL("IPL"), OWS(
				"OWS"), DIA("DIA"), _8XX("8XX"), LD("LD"), ELINE("ELINE"), NM(
				"NM"), QROUTE("QROUTE"), MS("MS"), ATM("ATM"), FRAME("FRAME"), DEDLD(
				"DEDLD"), VOIP("VOIP"), MOE("MOE"), DWH("DWH"), LSS("LSS"), LS("LS"), CE("CE"), ESP("ESP"),
				PLS("PLS"), IPS("IPS"), MNS("MNS"), VPNS("VPNS"), CDNS("CDNS"), IaaS("IaaS"), PaaS("PaaS"), SaaS("SaaS"), CHS("CHS"),
				IPSS("IPSS"), MSS("MSS"), SRE("SRE"), UCS("UCS"),MFWVPN("MFWVPN");

		ProductTypeCode(String value) {
			this.value = value;
		}

		private final String value;

		public String value() {
			return value;
		}
	}

	public enum SummaryFields {

		DEVICES("MS Devices"), ACCESSCIRCUITS("Access Circuits"), HOSTEDVOIP(
				"Hosted VoIP"), FIREWALL("Firewall"), ROUTER("Router"), SWITCH(
				"Switch"), UPS("UPS"), WAP("WAP"), WIFI("WiFi Controller");

		SummaryFields(String value) {
			this.value = value;
		}

		private final String value;

		public String value() {
			return value;
		}
	}

	public enum SummaryNames {

		SERVICE("SERVICE"),ACCESSCIRCUITS("ACCESSCIRCUITS"), DEVICE("DEVICE"), BA("BA");

		SummaryNames(String value) {
			this.value = value;
		}

		private final String value;

		public String value() {
			return value;
		}

	}
	public enum SubscriptionTypeCode {

		ALL("ALL"), SELECTED("SELECTED");

		SubscriptionTypeCode(String value) {
			this.value = value;
		}

		private final String value;

		public String value() {
			return value;
		}

	}
	public enum NotificationCategory {

		SERVICE_MANAGEMENT("Service Management"), BILLING("Billing"),ORDERS("Orders");

		NotificationCategory(String value) {
			this.value = value;
		}

		private final String value;

		public String value() {
			return value;
		}

	}

}
