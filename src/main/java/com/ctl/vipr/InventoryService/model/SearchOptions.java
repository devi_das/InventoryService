/*
 * 
 */
package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * @author kxbx
 * 
 */
public class SearchOptions implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected  transient final Logger log = LoggerFactory.getLogger(this.getClass());

	public static final String AND = " AND ";

	public static final String OR = " OR ";
	
	public static final String DELIMITER = ",";

	public static final String COLON = ":";

	public static final String AND_DELIMITER = ",";

	public static final String OR_DELIMITER = "|";
	
	public static final String CONDITIONAL_OPERATOR_EQUALS = ":";
	
	public static final String CONDITIONAL_OPERATOR_NOT_EQUALS = ":=";
	
	public static final String CONDITIONAL_OPERATOR_LESS_THAN = ":<";
	
	public static final String CONDITIONAL_OPERATOR_GREATER_THAN = ":>";
	
	public static final String CONDITIONAL_OPERATOR_GREATER_THAN_OR_EQUAL = ">=";
	
	public static final String CONDITIONAL_OPERATOR_LESS_THAN_OR_EQUAL = "<=";	
	
	public static final String CONDITIONAL_OPERATOR_IN = "#";

	public SearchFilterExpression sfe;
	
	public enum SearchQueryOperator {
		ET("ET"), IN("IN"), LIKE("LIKE"), BETWEEN("BETWEEN"), NE("NE"), LT("LT"), GT(
		"GT"), GE("GE"), LE("LE") ;

		SearchQueryOperator(String value) {
			this.value = value;
		}

		private final String value;

		public String value() {
			return value;
		}
	}

	private String searchText;

	private Calendar startDate;

	private Calendar endDate;

	private SearchQueryOperator searchQueryOperator;

	public SearchOptions() {
	}

	/*
	 * This constructor parses the searchString passed as parameters, forms a
	 * MainStream SearchOptions object for passing as parameter to the Service
	 * Interface. example searchString :
	 * serviceId:123fsd*;customName:Custom*;portType:IQ ENHANCED|IQ INTERNET;bandwidth:>6 Mbps;network:The 01 Network|SPRVU-10005388;serviceLocation:(locationStreet:930 15th Street,locationCity:Denver,locationStateProvince:CO,postalCode:80295)|(locationStreet:123 Test Dr,locationCity:Arvada)
	 * 
	 * serviceId:123fsd* AND customName:Custom* AND portType:(IQ ENHANCED OR IQ INTERNET) AND bandwidth:>6 Mbps AND network:(The 01 Network OR SPRVU-10005388) AND ((locationStreet:930 15th Street AND locationCity:Denver AND locationStateProvince:CO AND postalCode:80295) OR (locationStreet:123 Test Dr AND locationCity:Arvada))
	 */
	public SearchOptions(String searchString) {
		if (searchString == null) return;
		//follow the convention to have "COMPLEX|" prefix for the complex searches which allows wild card and special charactes in the search text..
		 if(searchString.contains(" AND ") || searchString.contains(" OR ")){
			sfe = new SearchFilterExpression(searchString);
		}
		
		this.searchText = searchString;
	}

	public SearchFilterExpression getExpression() {
		return sfe;
	}

	public void setExpression(SearchFilterExpression expression) {
		this.sfe = expression;
	}
	
	public String toString(){
		StringBuffer result = new StringBuffer();
		String newLine = System.getProperty("line.separator");
		result.append("SearchFilterExpression : ");
		if(null == sfe){
		result.append("null"); 
		}
		else{
			 result.append(sfe.toString());
		}
		result.append(newLine);
		result.append("searchText : "+searchText);
		result.append(newLine);
		result.append("startDate : "+startDate);
		result.append(newLine);
		result.append("endDate : "+endDate);
		result.append(newLine);
		return result.toString();
	}
	
	
	
	public static void main(String[] aArgs) {
//		String expre = "COMPLEX|abbrevItemName:*MSE: $whatever* AND portType:(IQ ENHANCED OR IQ INTERNET)";
		String expre = "COMPLEX|productType:SRE AND catalogLineItem.pricingElementCode:12 AND includeEndOfLife:True AND abbrevItemName:*MSE: $whatever* AND portType:(IQ ENHANCED OR IQ INTERNET)";
		SearchOptions soptions = new SearchOptions(expre);
		System.out.println(soptions.getExpression().toString());
	}
	
	/**
	 * @return the endDate
	 */
	public Calendar getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the searchQueryOperator
	 */
	public SearchQueryOperator getSearchQueryOperator() {
		return searchQueryOperator;
	}

	/**
	 * @param searchQueryOperator
	 *            the searchQueryOperator to set
	 */
	public void setSearchQueryOperator(SearchQueryOperator searchQueryOperator) {
		this.searchQueryOperator = searchQueryOperator;
	}

	/**
	 * @return the searchText
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * @param searchText
	 *            the searchText to set
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	/**
	 * @return the startDate
	 */
	public Calendar getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	
}
