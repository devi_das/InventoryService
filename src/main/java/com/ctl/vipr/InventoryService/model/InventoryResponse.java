package com.ctl.vipr.InventoryService.model;

public class InventoryResponse extends DAResponse{
	
	private static final long serialVersionUID = 1L;
	
	private String enterpriseId;
	private int totalNumberOfRecords;
	private int numberOfRecords;
	
	private String messageSelectorId;
		
	private InventoryItem[] inventory;
	
	public InventoryResponse(){
		super();
	}

	public InventoryResponse(DAResponse response){
		super(response);
	}

	public InventoryResponse(int status, String message) {
		super(status, message);
	}

	public String getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public int getTotalNumberOfRecords() {
		return totalNumberOfRecords;
	}

	public void setTotalNumberOfRecords(int totalNumberOfRecords) {
		this.totalNumberOfRecords = totalNumberOfRecords;
	}

	public int getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(int numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	public String getMessageSelectorId() {
		return messageSelectorId;
	}

	public void setMessageSelectorId(String messageSelectorId) {
		this.messageSelectorId = messageSelectorId;
	}

	public InventoryItem[] getInventory() {
		return inventory;
	}

	public void setInventory(InventoryItem[] inventory) {
		this.inventory = inventory;
	}

}
