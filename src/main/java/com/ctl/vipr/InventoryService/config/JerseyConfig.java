package com.ctl.vipr.InventoryService.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.ctl.vipr.InventoryService.controller.InventoryController;

@Component
public class JerseyConfig extends ResourceConfig{
//register all controllers here
	public JerseyConfig() {
		register(InventoryController.class);
	}
}
