package com.ctl.vipr.InventoryService.config;

import org.apache.cxf.binding.BindingConfiguration;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ctl.ecaps.service.inventory.InventoryService;

@Configuration
public class InventoryServiceConfig {

	@Value("${inventory.config.address}")
	private String address;
	
	@Value("${inventory.config.connectionTimeOut}")
	private long connectionTimeOut;
	
	@Value("${inventory.config.readTimeOut}")
	private long readTimeOut;
	
	@Bean
	public InventoryService inventoryServiceProxy(){
		
		JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
		jaxWsProxyFactoryBean.setServiceClass(InventoryService.class);
		jaxWsProxyFactoryBean.setAddress(address);
		InventoryService service =  (InventoryService) jaxWsProxyFactoryBean.create();
		
		Client client = ClientProxy.getClient(service);
        if (client != null) {
            HTTPConduit conduit = (HTTPConduit) client.getConduit();
            HTTPClientPolicy policy = new HTTPClientPolicy();
            policy.setConnectionTimeout(connectionTimeOut);
            policy.setReceiveTimeout(readTimeOut);
            conduit.setClient(policy);
        }
        return service;
	}
}
