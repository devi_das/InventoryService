package com.ctl.vipr.InventoryService.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ctl.ecaps.service.inventory.InventoryService;
import com.ctl.ecaps.serviceobject.InventoryRequestSO;
import com.ctl.ecaps.serviceobject.InventoryResponseSO;
import com.ctl.vipr.InventoryService.mapper.InventoryRequestMapper;
import com.ctl.vipr.InventoryService.mapper.InventoryResponseMapper;
import com.ctl.vipr.InventoryService.model.InventoryRequest;
import com.ctl.vipr.InventoryService.model.InventoryResponse;

@Component
@Path("/ctrl")
public class InventoryController {

	@Autowired
	InventoryService service;

	//@RequestMapping(value = "/inventory", method = RequestMethod.POST, consumes = "application/json")
	@POST
    @Produces("application/json")
    @Consumes("application/json")
	@Path("/inventory")
	public InventoryResponse getInventory(/*@RequestBody */InventoryRequest request){
		
		InventoryResponse resp = null;
		InventoryRequestMapper reqMapper = new InventoryRequestMapper();
		InventoryResponseMapper resMapper = new InventoryResponseMapper(); 
		InventoryRequestSO inventoryRequest = reqMapper.map("getInventory", request);
		
		try {
			long startTime = System.currentTimeMillis();
			InventoryResponseSO respSo = service.getInventory(inventoryRequest);
			long endTime = System.currentTimeMillis();
			resp = resMapper.mapResponse(respSo, request);
			resp.setNetworkResponseTime(endTime-startTime);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return resp;
	}
}
